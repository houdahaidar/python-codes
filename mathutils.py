import numpy as np
import math as m
# import m.pi as pi
# import np.sqrt as sqrt
# import np.log as log
# import np.log10 as log10

def fitpolyn(x,y,ord):
    """fit 2D polynomial of order ord to numpy array z(x,y)
    returns: coefficients (00 10 01 20 11 02 ...), total squared difference, rank, s
    Author: Gary Mamon (gam AAT iap.fr)"""
    A = np.zeros([len(x),ord+1])
    for pow in range(ord+1):
        A[:,pow] = x**pow
    return np.linalg.lstsq(A, y, rcond=None)[0]

def buildpolyn(x,coeff):
    """build polyomial from x and coefficient numpy arrays
    Author: Gary Mamon gam AAT iap.fr"""
    
    val = 0.
    for pow in range(0,len(coeff)):
        val = val + coeff[pow]*x**pow
    return val

def fitpolyn2d(x,y,z,ord):
    """fit 2D polynomial of order ord to numpy array z(x,y)
    returns: coefficients (00 10 01 20 11 02 ...), total squared difference, rank, s
    Author: Gary Mamon (gam AAT iap.fr)"""

    numsumpow = int(np.rint((ord+1)*(ord+2)/2))
    A = np.zeros([len(x),numsumpow])

    pow = 0
    for sumpow in range(ord+1):
        for powy in range(sumpow+1):
            powx = sumpow - powy
            A[:,pow] = x**powx * y**powy
            pow = pow + 1
    # print("A = ", A)
    return np.linalg.lstsq(A, z, rcond=None)
    
def buildpoly2d(x,y,coeffs):
    """build 2D polyomial from x and y numpy arrays and from coefficient numpy array
    Author: Gary Mamon gam AAT iap.fr"""
    
    ord = int(np.rint(-1.5+0.5*np.sqrt(9+8*len(coeffs))))
    val = 0.
    idx = 0
    for sumpow in range(ord+1):
        for powy in range(sumpow+1):
            powx = sumpow -powy
            val = val + coeffs[idx]* x **powx * y**powy
            idx = idx + 1
    return val

def NumPolyCoeffs(n):
    """number of coefficients of polynomial of order n
    Author: Gary Mamon (gam AAT iap.fr)"""
    
    return int(np.rint((n+1)*(n+2)/2))

def RMS(x):
    """root mean square of numpy array
    Author: Gary Mamon (gam AAT iap.fr)"""
    
    return np.sqrt(np.mean(x*x))


def ACO(X):
    """ArcCos for |X| < 1, ArcCosh for |X| >= 1
    arg: X (float, int, or numpy array)"""

    # author: Gary Mamon

    HUGE = 1.e30
    
    # following 4 lines is to avoid warning messages
    tmpX = np.where(X == 0, -1, X)
    tmpXbig = np.where(np.abs(X) > 1, tmpX, 1/tmpX)
    tmpXbig = np.where(tmpXbig < 0, HUGE, tmpXbig)
    tmpXsmall = 1/tmpXbig
    return ( np.where(np.abs(X) < 1,
                      np.arccos(tmpXsmall),
                      np.arccosh(tmpXbig)
                     ) 
           )

def StandardDevGapper(x):
    """Gapper measure of standard deviation
    Author: Gary Mamon (gam AAT iap.fr)
    Sources: Wainer & Thiussen (1976), Beers, Flynn & Gebhardt (1990), AJ 100, 32"""
    N = len(x)
    x1 = x[0] 
    x1 = np.append(x1,x)
    x2 = np.append(x,x[-1])
    diff = x2-x1
    diff = diff[1:-1]
    i = np.arange(1,N)
    weight = i*(N-i)
    return np.sqrt(np.pi) / (N*(N-1)) * np.sum(weight*diff)
