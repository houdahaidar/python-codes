import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import sys
import h5py
import requests
import weightedstats as ws
from statsmodels.stats.weightstats import DescrStatsW
from scipy.spatial import distance

#import illustris_python as il
from collections.abc import Iterable # for isinstance ... to check if param2 is scalar or array
home = os.getenv("HOME")
import illustris_python as il
from profcl import *
    
# plt.style.use('bmh')
# plt.style.use('/Users/gam/.matplotlib/stylelib/gam-dark.mplstyle')

baseUrl = 'http://www.tng-project.org/api/'

# Make sure you have a TNGTOKEN environment variable set
token = os.getenv("TNGTOKEN")
if token is None:
    raise ValueError("Need to set environment variable TNGTOKEN and relaunch Jupyter...")
headers = {"api-key":token}
# print("headers=",headers)

homedir = os.getenv("HOME")
datarootdir = homedir + "/SIMS/TNG/"
if os.path.isdir(datarootdir) == False:
    answer = input("Directory datarootdir is not present on your system: Create (y/n)? ")
    if answer in ['y','Y']:
        os.mkdir(datarootdir)
    else:
        rootdir = input("Enter root directory for TNG simulation data: ")
        datarootdir = homedir + rootdir
# print("past header...")

def get(path, params=None, verbose=0, savedir=None):
    # make HTTP GET request to path
    
    if verbose > 0:
        print("extracting ...")
    r = requests.get(path, params=params, headers=headers)
    
    # raise exception if response code is not HTTP SUCCESS (200)
    r.raise_for_status()

    if verbose > 0:
        print("get: r.headers=",r.headers)
        print("path = ",path)
        print('"ls -ltr | tail -3" gives:')
        os.system("ls -ltr | tail -3")
    if r.headers['content-type'] == 'application/json':
        return r.json() # parse json responses automatically

    if 'content-disposition' in r.headers:
        filename = r.headers['content-disposition'].split("filename=")[1]
        if savedir is not None:
            filename = savedir + "/" + filename
        if verbose > 0:
            print("filename = ",filename)
        with open(filename, 'wb') as f:
            if verbose > 0:
                print("writing in", filename,"...")
            f.write(r.content)

        return filename # return the filename string
    if verbose > 0:
        print("returning full request")
    return r

def getsims():
    """Extract list of Illustris (and TNG) simulations
    Author: Gary Mamon (gam AAT iap.fr)
    """
    r = get(baseUrl)
    sims = [sim['name'] for sim in r['simulations']]
    return sims

def getsim(simulation="TNG50-1"):
    """Extract simulation info
    Author: Gary Mamon (gam AAT iap.fr using Illustrus-TNG recipes)
    argument: simulation (default "TNG50-1") [string]
    returns info [dict]
    """
    r = get(baseUrl)
    sims = [sim['name'] for sim in r['simulations']]
    isim = sims.index(simulation)
    return get(r['simulations'][isim]['url'])

def zofsnap(snap,sim="TNG50-1"):
    """redshift of given snapshot number
    author: Gary Mamon (gam AAT iap.fr)"""
    sim_dict = getsim(sim)
    snap_dict = get(sim_dict['snapshots'])
    return snap_dict[snap]['redshift']
    
def zofsnaps(snaps,sim="TNG50-1"):
    """redshift(s) of given snapnum or array of snapnums
    author: Gary Mamon, gam AAT iap.fr
    """
    snaps_z_all = snapofz('all',simulation=sim)
    snaps_z_all.reverse()
    snaps_z_all = np.array(snaps_z_all)
    # print("snaps_z_all=",snaps_z_all)
    z = snaps_z_all[np.isin(snaps_z_all[:,0],snaps)][:,1]
    return z

def getsnapofz(z,simulation="TNG50-1",):
    """Extract snapnum info
    arguments:
        redshift
        simulation (default "TNG50-1") [string]
    returns info [dict]
    Author: Gary Mamon (gam AAT iap.fr using Illustris-TNG recipes)
    """
    s = getsim(simulation)
    url = s['snapshots'] + "z=" + str(z) + "/"
    snap = get(url)
    return snap

def snapofz(z='all',simulation="TNG50-1",show_z=False):
    """Extract snapnum of given redshift for given simulation
    arguments:
        redshift ('all' for all snapnums and corresponding redshifts)
        simulation (default "TNG50-1") [string]
        show_z: if True also return the redshift
    Author: Gary Mamon (gam AAT iap.fr using Illustrus-TNG recipes)
    """
    if z == 'all':
        sim = get(getsim(simulation)['snapshots'])
        snap_and_z = [(s["number"],s["redshift"]) for s in sim]
        return snap_and_z
    elif show_z:
        snap = getsnapofz(z,simulation=simulation)
        return snap['number'],snap['redshift']
    else:
        return getsnapofz(z,simulation=simulation)['number']

def gethalo(haloid,subhalo=False,simulation='TNG50-1',snapnum=99,parameter=None):
    """Extract halo parameters at given snapnum for given haloID or subhaloID (if subhalo=True)
    Author: Gary Mamon (gam AAT iap.fr)"""
    if subhalo:
        # extract haloID
        haloid = getsubhalo(haloid,simulation=simulation,snapnum=snapnum,parameter='grnr')

    url = 'http://www.tng-project.org/api/' + simulation + '/snapshots/' \
            + str(snapnum) + '/halos/' + str(haloid) + '/info.json'
    if parameter is None:
        return get(url)
    else:
        return get(url)[parameter]
    
def getsubhalo(subhaloid,simulation='TNG50-1',snapnum=99,snapmax=99,parameter=None,
               fromTree=False,treeMethod='sublink_mpb',datafileprefix=None,
               savedir=None,verbose=0):
    """Extract subhalo parameters at given snapnum
    Author: Gary Mamon (gam AAT iap.fr)"""
    if fromTree:
        if verbose > 0:
            print("in fromTree block")
        if savedir is None:
            savedir = os.getenv("HOME") + "/SIMS/TNG/" + simulation + "/output/"
        if verbose > 0:
            print("getsubhalo: savedir = ",savedir)
        if datafileprefix is None:
            # extract subhalo URL 
            if verbose > 0:
                print("extracting subhalo URL...")
            if snapnum < snapmax:
                subhaloid = getsubhaloid99(subhaloid,simulation=simulation,snapnum=snapnum,verbose=verbose)
                if verbose > 0:
                    print("subhaloid99 = "), subhaloid
            sub = getsubhalo(subhaloid,simulation=simulation,snapnum=snapmax,
                             savedir=savedir,verbose=verbose)
            progurl = sub['related']['sublink_progenitor']
            if progurl is None:
                print("no progenitor for subhalo", subhaloid)
                return
            
            # extract tree of main progenitor
            if verbose > 0:
                print("extracting tree of main progenitor...")
            datafile = get( sub['trees'][treeMethod] )
            if verbose > 0:
                print("datafile = ", datafile)
        elif datafileprefix in [0,'a',"auto"]:
            datafile = "sublink_mpb_" + str(subhaloid) + ".hdf5"
        # elif datafileprefix = 'groups99':
        #     datafile = 'groups_099/'
        else:
            datafile = datafileprefix + ".hdf5" 
        # extract values
        if verbose > 0:
            print("extracting parameters from tree in file " + datafile + "...")
        try:
            f = h5py.File(datafile,'r')
        except:
            raise FileNotFoundError(datafile)
        fsnapnum = f.loc[f['snapnum']==snapnum]
        if parameter == None:
            return fsnapnum
        else:
            return fsnapnum[parameter]
    else:
        url = 'http://www.tng-project.org/api/' + simulation + '/snapshots/' + str(snapnum) + '/subhalos/' + str(subhaloid)
        if verbose > 0:
            print("in regular block: url =",url)
        if parameter is None:
            return get(url)
        elif parameter in ["ssfr","sSFR"]:
            return get(url)['sfrinrad']/(1e10*get(url)['massinrad_stars'])
        else:
            return get(url)[parameter]

def getsubhalos(params,parammins,parammaxs,snapnum=99,basePath=home + "/SIMS/TNG/TNG50/output"):
    subhalos = il.groupcat.loadSubhalos(basePath=basePath,snapNum=snapnum,fields=params)
    # restrict to range of parameters
    
    if len(params) > 1:
        count = subhalos['count']
        i = np.arange(count).astype(int)
        # inew = np.zeros(len(params))
        for j, param in enumerate(params):
            print("param=",param)
            paramall = subhalos[param]
            # must do below np.where instead ...
            inew = i[(paramall >= parammins[j]) & (paramall <= parammaxs[j])]
            # take intersection of inew[j]s
            if j == 0:
                i2 = inew
            else:
                i2 = np.intersect1d(i2,inew,assume_unique=True)
            print("j len(i)=",j,len(i2))
        values = np.zeros((len(params),len(i2)))
        for j, param in enumerate(params):
            values[j,:] = subhalos[param][i2]
    else:
        values = subhalos[(subhalos>=parammins[0]) & (subhalos <= parammaxs[0])]
    return values

def getMPB(subhaloid,snapnum=99,sim='TNG50-1',verbose=0):
    """Get main progenitor branch
    Arguments:
        subhaloid: subhalo Subfind_ID (at snapshot = snapnum)
        snapnum: snapshot number [default 99]
        sim: simulation [default TNG50-1]
    Returns: dictionary-like h5py._hl.files.File 
    
    Author: Gary Mamon (gam AAT iap.fr)
    """
    
    filearg =  sim + "/snapshots/" + str(snapnum) + "/subhalos/" + str(subhaloid) + "/sublink/"
    file = datarootdir + filearg + "sublink_mpb_"+ str(subhaloid) + ".hdf5"
    if verbose > 0:
        print("attempting to read from file=",file)
    savedir = datarootdir + sim + "/snapshots/" + str(snapnum) + "/subhalos/" + str(subhaloid) + "/sublink/"
    ok = 1
    try:       
        os.chdir(savedir)
    except:
        if verbose > 0:
            print("cannot change directory to ", savedir)
        try:
            os.makedirs(savedir)
        except FileExistsError:
            # directory already exists
            pass
        except:
            print("cannot mkdir", savedir)
            ok = 0
        if ok == 1:
            if verbose > 1:
                print('changing cirectory to ',savedir)
            try:
                os.chdir(savedir)
            except:
                if verbose > 0:
                    print("failed to chdir to ", savedir)
                ok = 0
    if ok == 1:
        try:
            f = h5py.File(file, 'r')
        except:
            if verbose > 0:
                print("cannot read file", file)
            ok = 0
    if ok == 0:
        url = baseUrl + filearg + "mpb.hdf5"
        if verbose > 0:
            print("reading from TNG database",url," (saving to ",os.getcwd() + ") ...")
        mpb = get(url)
        f = h5py.File(mpb,'r')
            
    return f

def getMDB(subhaloid,snapnum=50,sim='TNG50-1',extract=False,verbose=0):
    """Get main descendant branch
    Arguments:
        subhaloid: subhalo Subfind_ID (at snapshot = snapnum)
        snapnum: snapshot number [default 50]
        sim: simulation [default TNG50-1]
    Returns: dictionary-like h5py._hl.files.File 
    
    Author: Gary Mamon (gam AAT iap.fr)
    """
        
    filearg =  sim + "/snapshots/" + str(snapnum) + "/subhalos/" + str(subhaloid) + "/sublink/"
    file = datarootdir + filearg + "sublink_mdb_"+ str(subhaloid) + ".hdf5"
    if (verbose > 0) & (not extract):
        print("attempting to read from file=",file)
    savedir = datarootdir + sim + "/snapshots/" + str(snapnum) + "/subhalos/" + str(subhaloid) + "/sublink/"
    ok = 1
    try:       
        os.chdir(savedir)
    except:
        if verbose > 0:
            print("cannot change directory to ", savedir)
        try:
            os.makedirs(savedir)
        except FileExistsError:
            # directory already exists
            pass
        except:
            print("cannot mkdir", savedir)
            ok = 0
        if ok == 1:
            if verbose > 1:
                print('changing cirectory to ',savedir)
            try:
                os.chdir(savedir)
            except:
                if verbose > 0:
                    print("failed to chdir to ", savedir)
                ok = 0
    if (ok == 1 ) & (not extract):
        try:
            f = h5py.File(file, 'r')
        except:
            if verbose > 0:
                print("cannot read file", file)
            ok = 0
    if (ok == 0) or (extract):
        url = baseUrl + filearg + "mdb.hdf5"
        if verbose > 0:
            print("reading from TNG database " + url + " (saving to ",os.getcwd() + ") ...")
        mdb = get(url)
        f = h5py.File(mdb,'r')
            
    return f

# constants
# simvals = getsim()
# Omegam0 = simvals['omega_0'] 
# h = simvals['hubble']

def Type2Str(Type):
    if Type == 0:
        name = "gas"
    elif Type == 1:
        name = "dm"
    elif Type == 4:
        name = "stars"
    elif Type == 5:
        name = "bh"
    else:
        raise ValueError("cannot recognize Type=",Type)
    return name

def saveParticles(subhalo,sim="TNG50-1",snapnum=99,request=None,savedir=None,
                  verbose=0):
    """ Download and save particle cutout for one subhalo. """
    baseUrl = 'http://www.tng-project.org/api/'

    sub_prog_url = baseUrl+sim+"/snapshots/"+str(snapnum)+"/subhalos/"+str(subhalo)+"/"
    sub_prog = get(sub_prog_url)
    if verbose > 0:
        print("sub_prog_url = ", sub_prog_url)
    if request is None:
        cutout_request = {'dm':'Coordinates','stars':'Coordinates,Masses'}
    else:
        cutout_request = request
    if verbose > 0:
        print("cutout_request = ", cutout_request)
    cutout = get(sub_prog_url + "cutout.hdf5", cutout_request)
    if verbose > 0:
        print("type(cutout)=",type(cutout))
        print("cutout=",cutout)

    if savedir is None:
        savedir = \
            home + "/SIMS/TNG/" + sim + "/snapshots/" + str(snapnum) + "/suhalos/" + str(subhalo) 
    try:
        os.makedirs(savedir)
    except:
        # directory already exists
        pass
    final_filename = savedir + "/cutout_" + sim + "_" + str(snapnum) + "_" + str(subhalo) + ".hdf5"
    if verbose > 0:
        print("filename is now", final_filename)
    os.rename(cutout, final_filename)

def particles(subhalo,sim='TNG50-1',snapnum=99,PartType=4,params=["Coordinates","Velocities","Masses"],
              savedir=None,extract=False,retry=0,verbose=0):
    """
    Particle data

    Arguments:
    subhalo : subhaloID
    sim : simulation (default ("TNG50-1")
    snapnum : snapshot number (default 99)
    PartType : particle type number (default 4 for stars)
    params : parameters to study (default "Coordinates")
    savedir : directory where particle data is stored (default ".")
    extract : extract from TNG server? (default False)
    verbose : verbosity (default 0)

    Returns dict of particle data 

    Author: Gary Mamon (gam AAT iap.fr), inspired by TNG guidelines
    with help from Houda Haidar
    """
    if (retry == 1) & (verbose > 0):
        print("retrying ...")
    if savedir is None:
        savedir = \
            home + "/SIMS/TNG/" + sim + "/snapshots/" + str(snapnum) + "/subhalos/" + str(subhalo) 
    try:
        os.makedirs(savedir)
    except:
        # directory already exists
        pass
    if extract: 
        # extract specific particle parameter values and save to disk
        paramsString = ','.join(params)
        if verbose > 1:
            print("paramsString=",paramsString)
        cutout_request = {Type2Str(PartType):paramsString}
        if verbose > 0:
            print("extracting for subhalo",subhalo,"with cutout_request=",
                  cutout_request)
        saveParticles(subhalo,sim=sim,snapnum=snapnum,request=cutout_request,
                      savedir=savedir,verbose=verbose)

    # read file of particle parameter values
    filename = savedir + "/cutout_" + sim + "_" + str(snapnum) + "_" + str(subhalo) + ".hdf5"
    if verbose > 0:
        print("extracting data from", filename,"...")
    try:
        f = h5py.File(filename,'r')
    except:
        print("cannot open file",filename,"..., extracting from TNG database...")
        # extract from TNG database
        return particles(subhalo,sim=sim,snapnum=snapnum,PartType=PartType,params=params,
                  savedir=savedir,extract=True,retry=1,verbose=verbose)
        # saveParticles(subhalo,sim=sim,snapnum=snapnum,request=params)
    if verbose > 0:
        print("f.keys()=",f.keys())
    groupname= "PartType" + str(PartType)
    if groupname not in f:
        print("groupname",groupname,"not in f")
        print("f['Header'].keys() = ",f['Header'].keys())
        return {}
    data = {}
    if isinstance(params,str):
        if params in f[groupname]:
            data = f[groupname][params]
        else:
            print("parameter",params,"not found in",groupname)
            return {}      
    else:
        for i, param in enumerate(params):
            if param in f[groupname]:
                data[param] = f[groupname][param][:]
            else:
                print("parameter",param,"not found in",groupname)
                print("choices are",f[groupname].keys())
                return {}
    return data
    f.close()

def ConvertDict(dict,df=False):
    """convert dict to new dict (or pandas dataframe) so that all 2D data become 1D"""
    dictNew = {}
    for k,key in enumerate(dict):
        if isinstance(dict[key][0],np.ndarray):
            keylen2 = len(dict[key][0])
            for i in range(keylen2):
                key2 = key + str(i)
                dictNew[key2] = dict[key][:,i]
        else:
            dictNew[key] = dict[key]
    print("ConvertDict: df = ",df)
    if df:
        return pd.DataFrame.from_dict(dictNew)
    else:
        return dictNew
    
def getsubhaloid99(subhalo,simulation="TNG50-1",snapnum=None,verbose=0):
    if simulation.find("TNG") != 0:
        raise ValueError("cannot run getsubhaloid99 for non-TNG simulation... use getsubhaloid_z0")
    if verbose > 0:
        print("snapnum=",snapnum,"subhalo=",subhalo)
    if snapnum is None:
        raise ValueError("snapnum must be given (integer from 0 to 98)")
    elif snapnum == 99:
        return subhalo
    elif snapnum < 99:
        subhalodesc = getsubhalo(subhalo,simulation=simulation,snapnum=snapnum
                                 ,parameter="desc_sfid")
        if subhalodesc==-1:
            print("subhalo",subhalo,"at snapnum",snapnum,"has no descendant")
            return -1
        else:
            return getsubhaloid99(subhalodesc,simulation=simulation,snapnum=snapnum+1,
                                 verbose=verbose)

def getsubhaloid_z0(subhalo,simulation="TNG50-1",snapnum=None,verbose=0):
    """get z=0 subhaloid
    arguments:
        subhalo: subhalo ID
        simulation
        snapnum    
    returns subhalo id at final snapnum
    author: Gary Mamon (gam AAT iap.fr)
        """
    snapnumz0 = snapofz(0,simulation=simulation)
    if verbose > 0:
        print("snapnum=",snapnum,"subhalo=",subhalo)
    if snapnum is None:
        raise ValueError("snapnum must be given (integer from 0 to 98)")
    elif snapnum == snapnumz0:
        return subhalo
    elif snapnum < snapnumz0:
        subhalodesc = getsubhalo(subhalo,simulation=simulation,snapnum=snapnum
                                 ,parameter="desc_sfid")
        if subhalodesc==-1:
            print("subhalo",subhalo,"at snapnum",snapnum,"has no descendant")
            return -1
        else:
            return getsubhaloid_z0(subhalodesc,simulation=simulation,snapnum=snapnum+1,
                                 verbose=verbose)

def SubhaloCenswGroupParams(paramsSubhalos=None,paramsHalos=None,sim="TNG50-1",
                            snapnum=99,verbose=0):
    """Produce dataframe with selected parameters from both subhalo centrals and Groups

    Parameters:
        paramsSubhalos: list of Subhalo table keys (None for all)
        paramsHalos: list of Group table keys (None for all)
        sim: TNG simulation (default: "TNG50-1")
        snapnum: snap number (default 99)
        verbose: verbosity (default 0 for no special output)

    Returns: merged dataframe

    Author: Gary Mamon (with help grom Houda Haidar)
    """
    basePath = home + "/SIMS/TNG/" + sim + "/output"
    
    # subhalos
    print("paramsSubhalos is first",paramsSubhalos)
    if 'SubhaloGrNr' not in paramsSubhalos:
        paramsSubhalos.append('SubhaloGrNr')
    if verbose > 0:
        print("subhalos...")
        print("paramsSubhalos=",paramsSubhalos)
    subs = il.groupcat.loadSubhalos(basePath=basePath,snapNum=snapnum,fields=paramsSubhalos)
    if verbose > 0:
        print("converting to df...")
    dfSubs = ConvertDict(subs,df=True)
    
    # groups
    print("paramsHalos is first",paramsHalos)
    if 'GroupFirstSub' not in paramsHalos:
        paramsHalos.append('GroupFirstSub')
    if verbose > 0:
        print("groups...")
        print("paramsHalos=",paramsHalos)
    groups = il.groupcat.loadHalos(basePath=basePath,snapNum=snapnum,fields=paramsHalos)
    if verbose > 0:
        print("converting to df...")
    dfGroups = ConvertDict(groups,df=True)
    dfGroupsClean = dfGroups.loc[dfGroups.GroupFirstSub >= 0]
    # restrict subhalos to the centrals in dfGroups
    dfSubs_tmp = dfSubs.iloc[dfGroupsClean.GroupFirstSub]
    
    # merge
    if verbose > 0:
        print("merging...")
    dfm = pd.merge(dfSubs_tmp,dfGroupsClean,how='inner',left_on='SubhaloGrNr',right_index=True)
    return dfm

def indices(dict,param,valuemin,valuemax):
    """Convert dict to dataframe    
    returns indices as numpy array
    Author: Gary Mamon (gam AAT iap.fr) and Yuankang Liu (yuankang.liu AAT gmail.com)"""
    df = ConvertDict(dict,df=True)
    return df.index[(df[param]>=valuemin) & (df[param]<=valuemax)].values

def BoxSize(sim='TNG50-1'):
    """
    box size of simulation (Mpc/h)
    argument: simulation (default: 'TNG50-1')
    author: Gary Mamon (gam AAT iap.fr)
    """
    Lbox_dict = {'TNG50-1':35,'TNG100-1':75,'TNG300-1':205}
    if sim in Lbox_dict.keys():
        size = Lbox_dict[sim]
    else:
        raise ValueError("box size of " + sim + " is not known!")
    return size

def FixPeriodic(dx,sim='TNG50-1'):
    """
    Handle periodic boundary conditions
    Arguments:
        dx: difference in positions (in kpc/h)
        sim: simulation (default "TNG50-1")

    Returns: dx corrrected for periodic box
    Author: Gary Mamon (gam AAT iap.fr)
    """
    L = 1000 * BoxSize(sim) # BoxSize in Mpc, L in kpc
    # L = getsim(sim)['boxsize']
    dx = np.where(dx>L/2,dx-L,dx)
    dx = np.where(dx<-L/2,dx+L,dx)
    return dx

def RadialCoordinate(pos,posRef,sim='TNG50-1',verbose=0):
    """Radial coordinate, correcting for periodic box
    Author: Gary Mamon (gam AAT iap.fr)"""
    pos = np.array(pos)
    if pos.shape[-1] != 3:
        raise ValueError("pos must have a shape of 3 or N,3")
    if len(posRef) != 3:
         raise ValueError("posRef must have a length of 3")       
    dpos = []
    for i in range(3):
        if pos.ndim == 2:
            pos_tmp = pos[:,i]
        else:
            pos_tmp = pos[i]
        dpos_raw = pos_tmp - posRef[i]
        # correct for possible system at edge of box (periodic boundary conditions)
        dpos.append(FixPeriodic(dpos_raw,sim=sim))
    dpos = np.array(dpos)
    return np.sqrt(dpos[0]*dpos[0] + dpos[1]*dpos[1] + dpos[2]*dpos[2])

def VelSubhaloInGroup(velSub,velGroup,sim='TNG50-1',snap=99,verbose=0):
    """Relative peculiar velocity of subhalo in group frame
    Author: Gary Mamon (gam AAT iap.fr)"""
    z = zofsnap(snap,sim=sim)
    a = 1/(1+z)
    return np.array(velSub) - np.array(velGroup)/a

def VelPartInSubhalo(velPart,velSub,sim='TNG50-1',snap=99):
    """Relative peculiar velocity of particle in subhalo frame
    Author: Gary Mamon (gam AAT iap.fr)"""
    z = zofsnap(snap,sim=sim)
    a = 1/(1+z)
    return np.sqrt(a)*np.array(velPart) - np.array(velSub)
    
def tickInterval(deltax,numticks=5):
    """Tick interval given tick limits
    Author: Gary Mamon, gam AAT iap.fr"""
    n = [1,2,3,4,5]
    logn = np.log10(n)
    dx = deltax/numticks
    logdx = np.log10(dx)
    logdx_mantissa = logdx - np.floor(logdx)
    logdx_exponent = logdx - logdx_mantissa
    absdiff = np.abs(logn - logdx_mantissa)
    return n[np.argmin(absdiff)]*10**logdx_exponent

def CleanTicks(xmin,xmax,dx=None,numticks=5):
    """Return optimal ticks given min and max values
    author: Gary Mamon, gam AAT iap.fr """
    if dx is None:
        dx = tickInterval(xmax-xmin,numticks=numticks)
    ticks = dx*np.arange(np.floor(xmin/dx),np.floor(xmax/dx)+1)
    ticks = ticks[(ticks>= xmin) & (ticks <= xmax)]
    return ticks

def VelocityField(subhaloid,sim='TNG50-1',snap=99,roverrhalf=2,rshell=0,droverrhalf=0.2,
                  drshell=0,partType=0,axis='xy',
                  savedirroot='SIMS/TNG',savefig=None,
                  alpha=1,title='auto',verbose=0):
    """

    Parameters
    ----------
    subhaloid: TYPE
        subhalo id
    sim : TYPE, optional
        simulation. The default is 'TNG50-1'.
    snap : TYPE, optional
        snapshot number. The default is 99.
    roverrhalf : TYPE, optional
        r_shell / r_half. The default is 2. (0 to use rshell)
    rshell : TYPE, optional
        radius of shell (in ckpc/h). The default is 0. (then uses roverrhalf)
    droverrhalf : TYPE, optional
        thickness of shell (in half-mass radii). The default is 0.2.
    drrshell : TYPE, optional
        thickness of shell (in ckpc/h). The default is 0. (then uses roverrhalf)
    partType : TYPE, optional
        particle type (0 for gas, 1 for dark matter, 4 for stars). The default is 0.
    axis: 
        sky axis, default: 'xy'
    savefig:
        'auto': automatic filename
        otherwise filename,
        default None
    alpha:
        opacity for plot (default 1)
    title:
        plot title (default false)
            'auto': automatic title
            otherwise title
            default None
    verbose:
        verbosity: 0 = no debugging output

    Returns
    -------
    plot

    """
    # global df, parts
    # Particles
    if verbose > 0:
        print("Extracting particles...")
    savedir = os.getenv("HOME") + '/' + savedirroot + '/' + sim
    parts = particles(subhaloid,sim,snapnum=snap,params=['Coordinates','Velocities','Masses'],
                  PartType=partType,savedir=savedir,verbose=0,extract=True)
    
    # Subhalo
    if verbose > 0:
        print("Extracting subhalo...")
    sub = getsubhalo(subhaloid,simulation=sim,snapnum=snap)
    sub['Pos'] = np.array([sub['pos_x'],sub['pos_y'],sub['pos_z']])
    tmpvel = [sub['vel_x'],sub['vel_y'],sub['vel_z']]
    sub['Vel'] = [sub['vel_x'],sub['vel_y'],sub['vel_z']]
    
    # Group
    if verbose > 0:
        print("Extracting group...")
    group_id = sub['grnr']
    group = gethalo(group_id,simulation=sim,snapnum=snap)
    
    # bulk velocity in Group
    vsub = sub['Vel']
    vgroup = group['GroupVel']
    sub['Vrel'] = VelSubhaloInGroup(sub['Vel'],group['GroupVel'],sim=sim,verbose=verbose)
    
    # radii 
    parts['r'] = RadialCoordinate(np.array(parts['Coordinates']),np.array(sub['Pos']),sim=sim,
                                  verbose=verbose)
        
    # relative velocities
    parts['Vrel'] = VelPartInSubhalo(parts['Velocities'],vsub,sim=sim,snap=snap)
        
    # convert to dataframe for easier selection
    df = ConvertDict(parts,df=True)
    if verbose > 1:
        print("df.keys = ",df.keys())
    
    # half-mass radius of partType
    if roverrhalf > 0:
        # mass weighted median
        rhalf = ws.weighted_median(df.r,df.Masses)
        rshell = rhalf * roverrhalf
        drshell = droverrhalf * rhalf
        
    # select shell particles
    dfshell = df.loc[np.abs(df.r-rshell) < drshell]
    
    # statistics
    dsw = DescrStatsW(df.r.values,weights=df.Masses.values)
    
    # velocity statistics
    # vrelmean = np.zeros(3)
    # for i in range(3):
    #     vrelmean[i] = np.average(dfshell.Vrel.values,weights=dfshell.Masses.values)
    vrelmean = np.average(dfshell.Vrel.values,axis=1,weights=dfshell.Masses.values)
    cos_vrel_vbulk = 1 - distance(vrelmean,np.array(sub['Vrel']))
    rsubinGroup = RadialCoordinate(sub['Pos'],group['GroupPos'])
    cos_ringroup_vrel = 1 - distance(vrelmean,rsubinGroup)
    vrel_r = np.sum(np.array(parts['Coordinates']),np.array(parts['Vrel'])) / parts['r']
    sigma_v_shell = np.std(parts['Vrel'],axis=1)
    vr_over_sigma = vrel_r/sigma_v_shell
    
    if verbose > 0:
        print("plotting...")
    if title == 'auto':
        title = sim + ' snap ' + str(snap) + ' subhalo ' + str(subhaloid) + ' gas'
    plotVelField(df,dfshell,sim=sim,snap=snap,alpha=alpha,title=title,
                 savefig=savefig)
    return cos_vrel_vbulk, vr_over_sigma, cos_ringroup_vrel
    
def plotVelField(df,dfshell,sim="TNG50-1",snap=99,axis='xy',alpha=1,title=None,
                 savefig=None):
    # plot velocity field
    if axis == 'xy':
        x = df.Coordinates0 / 1000
        y = df.Coordinates1 / 1000
        vx = df.Vrel0
        vy = df.Vrel1
        xs = dfshell.Coordinates0 / 1000
        ys = dfshell.Coordinates1 / 1000
        vxs = dfshell.Vrel0
        vys = dfshell.Vrel1
    elif axis == 'yz':
        x = df.Coordinates1 / 1000
        y = df.Coordinates2 / 1000
        vx = df.Vrel1
        vy = df.Vrel2
        xs = dfshell.Coordinates1 / 1000
        ys = dfshell.Coordinates2 / 1000
        vxs = dfshell.Vrel1
        vys = dfshell.Vrel2
    elif axis == 'xz':
        x = df.Coordinates0 / 1000
        y = df.Coordinates2 / 1000
        vx = df.Vrel0
        vy = df.Vrel2
        xs = dfshell.Coordinates0 / 1000
        ys = dfshell.Coordinates2 / 1000
        vxs = dfshell.Vrel0
        vys = dfshell.Vrel2
    else:
        raise ValueError("Cannot recognize axis = " + axis)
    xlabel = '$' + axis[0] + '$'
    ylabel = '$' + axis[1] + '$'
    # plt.figure(figsize=[5,5])
    plt.quiver(x,y,vx,vy,color='gray')
    plt.quiver(xs,ys,vxs,vys,color='r',alpha=alpha)
    plt.xlabel(xlabel + ' (cMpc/$h$)')
    plt.ylabel(ylabel + ' (cMpc/$h$)')
    ax = plt.gca()
    ax.set_aspect(1)
    ax.ticklabel_format(useOffset=False)
    partTypes = ['gas','dark matter', None, None, 'stars']
    if title is not None:
        plt.title(title)
    # plt.axis('scaled')
    if savefig == 'auto':
        plt.savefig(sim + '_' + str(snap) + '_' + str(subhalo) + '_vfield_' + axis + '.pdf')
    elif savefig is not None:
        plt.savefig(savefig + '.pdf')
        


def PlotHistory(y,x,subhaloid=None,sim=None,snapmin=None,snapmax=99,
                param=None,param2=None,yscale=None,
                labels=None,colors=None,ylabel=None,marker=None,legend=None,
                savefig=False):
    """Plot TNG history
    Author: Gary Mamon, gam AAT iap.fr"""
    snaps = range(snapmin,snapmax+1)
    snaps_z_all = np.array(snapofz('all'))
    z = snaps_z_all[np.isin(snaps_z_all[:,0],snaps)][:,1]
    t = AgeUniverse(Omegam0,h,z)
    print("len(t)=",len(t))
    print("len(y)=",len(y))
    
    fig = plt.figure()
    
    # first horizontal axis: times (age Universe)
    ax = fig.add_subplot(111)
    # ax.plot(t,y,marker='o')
    if legend:
        if param2 is not None:
            if isinstance(param2,Iterable): # param2 is a list or tuple or array
                for i, par2 in enumerate(param2):
                    ax.plot(t,y[:,i],label=labels[par2],
                             color=colors[par2],marker=marker)
            else: # param2 is a scalar
                par2 = param2
                ax.plot(t,y[:],color=colors[par2],marker=marker)
                legend = False
        else: # param2 is not given
            for i in range(len(labels)):
                if ((param.find("Type") > 0) & (i in [2,3])):
                    continue
                ax.plot(t,y[:,i],label=labels[i],
                         color=colors[i],marker=marker)
        # check again legend, which may have been turned off in the block above
        if legend:
            if ((len(labels) <= 4) or (param.find("Type") > 0)):
                fsize = 16
            else:
                fsize = int(32/np.sqrt(len(labels)))
            print("len(labels)=",len(labels))
            print("legend fontsize = ",fsize)
#             if param == "SubhaloStellarPhotometrics":
#                 print("small legend?")
#                 plt.legend(fontsize="small")
#             else:
            plt.legend(fontsize=fsize)
    elif param in ["xy","xz","yz"]:
        ax.plot(x,y,marker=marker)
        ax.scatter([x[0]],[y[0]],marker='o',c='k',s=150)
        ax.text(x[0],y[0],"99",c='orange',fontsize=10,
                 horizontalalignment='center',verticalalignment='center')
    else: # Type is not in parameter name
        ax.plot(t,y,marker=marker)
        if param in ["mass","Mass"]:
            plt.legend()
    if ylabel:
        ax.set_ylabel(ylabel)
    ax.set_yscale(yscale)
    ax.grid(True)
    if subhaloid:
        plt.title(sim + "  subhalo " + str(subhaloid), fontsize=18)
    
    # 2nd horiontal axis (top of box): redshifts
    ax2 = ax.twiny()
    zticks = CleanTicks(0,np.max(z))
    tticks = AgeUniverse(Omegam,h,zticks)
    zticks = zticks.tolist()
    tticks = tticks.tolist()
    zlabels = ['%.2f' % z for z in zticks]
    axlims = ax.get_xlim()
    ax2.set_xlim(axlims)
    ax2.set_xticks(tticks)
    ax2.set_xticklabels(zlabels)
    ax2.set_xlabel('redshift')
    ax.set_xlabel('time (Gyr)')
    # plt.grid(None)
    # ax.grid(True)
    ax2.minorticks_off()
    ax2.grid(None)
    
    # 3rd horizonatal axis: snapnums
    ax3 = ax.twiny()
    # Add some extra space for the second axis at the bottom
    fig.subplots_adjust(bottom=0.2)
    # Move twinned axis ticks and label from top to bottom
    ax3.xaxis.set_ticks_position("bottom")
    ax3.xaxis.set_label_position("bottom")
    # Offset the twin axis below the host
    ax3.spines["bottom"].set_position(("axes", -0.25))
    snaps4labels = CleanTicks(snapmin,snapmax)
    if 99 not in snaps4labels:
        snaps4labels = np.append(snaps4labels,99)
    zsnaps = snaps_z_all[np.isin(snaps_z_all[:,0],snaps4labels)][:,1]
    tticks2 = AgeUniverse(Omegam0,h,np.array(zsnaps))
    snaplabels = ['%d' % s for s in snaps4labels]
    ax3.set_xlim(axlims)
    ax3.set_xticks(tticks2)
    ax3.set_xticklabels(snaplabels)
    ax3.set_xlabel('snapnum')
    ax3.grid(None)
    ax3.minorticks_off()
    
    # finalize plot
    if savefig:
        plt.tight_layout()
        plt.savefig(savefig + ".pdf")
    else:
        plt.show()
        
    return t
    
def NewHistory(subhaloid,param='SFR',param2=None,sim='TNG50-1',treeMethod='sublink_mpb',
                snapmin=None,snapmax=99,snapnum=99,datafileprefix=None,plot=True,
                ylabel=None,yscale='log',ylims=None,datarootdir=None,
                verbose=0,savefig=False,usetex=True,marker=None):
    """Extract and optionally plot evolution of subhalo parameter
    arguments:
        subhaloid
        param=param, where param is one of the subhalo attributes, or shortcuts
            "Mass" (stellar mass within 2 r_h)
            "SFR" (SFR within 2 r_h)
            "sSFR" (sSFR = SFR/Mass_Stars, both within 2 r_h)
        param2=num, where num is the type for Mass (e.g. 4 for Stars)
        snapmax: highest (latest, i.e. base) snapnum (default 99)
        snapmin: lowest (earliest) snapnum (default None last plus one)
        snapnum: reference snapnum for subhaloid (default snapmax)
        datafileprefix: data file prefix (0 or "auto" for sublink_mpb_[subhaloid], None to extract (default))
        plot: do-plot? [boolean]
        ylabel: y-label
        yscale: scale of y axis
        ylims: [ymin,ymax]
        savefig: output file of figure
        usetex: use TeX fonts?
        marker: marker (default None)
    Author: Gary Mamon (gam AAT iap.fr)"""
    
    # dimensionless Hubble constant
    h = 0.6774
    
    if datarootdir is None:
        datarootdir = os.getenv("HOME") + "/SIMS/TNG/"
        # answer = input("Enter root directory for TNG simulations: [" + datarootdir + "]")
        # if answer != '':
        #     datarootdir = answer
    datadir = datarootdir + sim + "/output/"
    if verbose > 0:
        print("History: datadir=",datadir)
    if datafileprefix is None:
        # extract subhalo URL 
        print("extracting subhalo URL...")
        if snapnum < snapmax:
            subhaloid = getsubhaloid99(subhaloid,simulation=sim,snapnum=snapnum,verbose=verbose)
            if verbose > 0:
                print("at snapnum=99, subhaloid = ",subhaloid)
        if verbose > 0:
            print("getsubhalo...")
        sub = getsubhalo(subhaloid,simulation=sim,snapnum=snapmax,
                         savedir=datadir,verbose=verbose)
        if verbose > 1:
            print ("History: sub=",sub)
        progurl = sub['related']['sublink_progenitor']
        if progurl is None:
            print("no progenitor for subhalo", subhaloid)
            return
        # extract tree of main progenitor
        if verbose > 0:
            print("extracting tree of main progenitor with method",treeMethod)
            print("... and saving tree to", datadir)
            print("progurl = ",progurl)
        datafile = get(sub['trees'][treeMethod],savedir=datadir)
    elif datafileprefix in [0,'a',"auto"]:
        datafile = datadir + "sublink_mpb_" + str(subhaloid) + ".hdf5"
    else:
        datafile = datadir + datafileprefix + ".hdf5" 
    if verbose > 0:
        print("cwd=",os.getcwd())
        os.system("ls -l " + datafile)
    
    # extract values
    if verbose > 0:
        print("extracting parameters from tree in file " + datarootdir + datafile + "...")
    try:
        f = h5py.File(datafile,'r')
        if verbose > 0:
            print("done")
    except:
        # recursive relaunch with datafileprefix=None to force read from TNG database
        print("file ", datafile, " not found on disk, trying TNG database...")
        History(subhaloid,param=param,param2=param2,sim=sim,treeMethod=treeMethod,
                snapmin=snapmin,snapmax=snapmax,snapnum=snapnum,datafileprefix=None,
                plot=plot,
                xlabel=xlabel,ylabel=ylabel,yscale=yscale,ylims=ylims,
                verbose=verbose,savefig=savefig,usetex=usetex,marker=marker)
    if verbose >= 1:
        print("type(f)=",type(f))
        print("f.keys=",f.keys())
    snapnums = f['SnapNum'][:]
    x = snapnums.copy()
#     if snapmin is None:
#         if verbose > 0:
#             print("setting snapmin to ",snapnum[-1]-1)
#         snapmin = snapnum[-1]-1
    if verbose > 0:
        print("first 2 SubhaloID = ",f['SubhaloID'][0:2])
        print("first 2 SubfindID = ",f['SubfindID'][0:2])

    # special treatment for specific paramters
    substr = "sSFR"
    isSFR = param.index(substr) if substr in param else -1
    if param in ["sBHmdot"]: # special treatment for specific black hole mass growth rate
        # use 2 R_e aperture
        mdotBH = np.asarray(f['SubhaloBHMdot'])
        massBH= np.asarray(f['SubhaloBHMass'])
        sBHMdot = mdotBH/massBH
        y = np.log10(sBHMdot)
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "$\log\,[\dot M_\mathrm{BH}/M_\mathrm{BH}]\ (\mathrm{0.978 Gyr}^{-1})$"
            else:
                ylabel = "log [Mdot_BH/M_BH] [1/0.978 Gyr]"
    elif param in ["fgas"]: # special treatment for sSFR like param name
        # use 2 R_e aperture
        massAll = f['SubhaloMassInRadType']
        massStars = massAll[:,4]*1e10
        massGas = massAll[:,0]*1e10
        y = np.log10(massGas/massStars) # independent of h
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "$\log\,(M_\mathrm{gas}/M_\mathrm{stars})$"
            else:
                ylabel = "log M_gas/M_stars"
    elif param in ["sSFR","ssfr","SSFR"]: # special treatment for sSFR like param name
        # use 2 R_e aperture
        sfr = f['SubhaloSFRinRad']
        massStars = f['SubhaloMassInRadType']
        massStars = massStars[:,4]*1e10
        y = np.log10(sfr/massStars)
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "$\log\,[\mathrm{sSFR}\,(2\,R_\mathrm{e})]\ (\mathrm{yr}^{-1})$"
            else:
                ylabel = "log sSFR (2 R_e) [1/yr]"
    elif param in ["sfr",'SFR']: # special treatment for SFR like param name
        # use 2 R_e aperture
        sfr = f['SubhaloSFRinRad']
        y = np.log10(sfr[:,])
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "$\log\,[\mathrm{SFR}\,(2\,R_\mathrm{e})]\ (\mathrm{M_\odot\,yr}^{-1})$"
            else:
                ylabel = "log SFR (2 R_e) [M_sun/yr]"
    elif param in ["mass","Mass"]: # special treatment for mass-like param name
        mass = f['SubhaloMass']
        y = np.log10(1e10 * mass[:] / h)
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "$\log\,[\mathrm{mass}_\mathrm{subhalo})]\ (\mathrm{M}_\odot)$"
            else:
                ylabel = "log mass (subhalo) [M_sun]"
    elif param in ["mass2Re","Mass2Re"]: # special treatment for mass like param name
        mass = f['SubhaloMassInRadType']
        y = np.log10(1e10 * mass[:,:] / h)
        yscale = 'linear'
        param2 = (0,1,4,5)
        if ylabel is None:
            if usetex:
                ylabel = "$\log\,[\mathrm{mass}\ (2\,R_\mathrm{e})]\ (\mathrm{M}_\odot)$"
            else:
                ylabel = "log mass (2 R_e) [M_sun]"
    elif param in ["Group_M_Crit200","Group_R_Crit200",
                  "Group_M_Crit500","Group_R_Crit500"]:
        ytmp = f[param]
        if param[6] == 'M':
            y = np.log10(1e10 * ytmp[:] / h)
        else:
            y = ytmp
        yscale = 'linear'
        ylabel = param.replace('Crit200','\mathrm{200,c}')
        ylabel = ylabel.replace("Crit500","\mathrm{500,c}")
        if param[7:1] == 'M':
            ylabel = ylabel + ' ($mathrm{M}_\odot$)'
        else:
            ylabel = ylabel + ' (kpc)'
    elif param in ["r_over_R_Crit200","r_over_R_Crit500"]:
        paramRef = "Group_" + param[7:]
        yRef = f[paramRef]
        pos = f['SubhaloPos']
        posGroup = f['GroupPos']
        y = np.zeros(len(pos[:]))
        if verbose > 0:
            print("pos[:,:] = ",pos[:,:]) 
            print("posGroup[:,:]=",posGroup[:,:])
            print("yRef = ", yRef[:])
        # r^2 = dx^2 + dy^2 + dz^2
        for j in range(3): # loop over cartesian axes
            dr = FixPeriodic(pos[:,j]-posGroup[:,j],sim)
            y = y + dr*dr
        # r / R_vir (independent of h)
        y = np.sqrt(y) / yRef
        yscale = 'log'
        ysuf = param[7:].replace('Crit200','\mathrm{200,c}')
        ysuf = ysuf.replace("Crit500","\mathrm{500,c}")
        ylabel = "$r/" + ysuf + "$"
    elif param[-3:] in ["Pos","pos"]:
        pos = f[param]
        y = pos[:]-pos[0,:]
        # handle periodic boundary conditions
        y = FixPeriodic(y,sim) / h
        yscale = 'linear'
        ylabel = "$\mathrm{" + param + " - " + param + "}$[99] (kpc)"
    elif param[-3:] in ["PosRel","posrel"]:
        # show positions relative to group
        pos = f[param]
        posGroup = f['GroupPos']
        y = pos - posGroup
        # watch for crossing of box boundaries
        y = FixPeriodic(y, sim) / h
        yscale = 'linear'
        ylabel = "$\mathrm{" + param + " - " + param + "_\mathrm{group}}$ (kpc)"
    elif param[-3:] in ["Vel","vel"]:
        # NEED TO RESCALE BY sqrt(a)
        vels = f[param]
        y = vels[:]
        yscale = 'linear'
        ylabel = '$\mathrm{' + param + "\ (km\ s^{-1})}$"
    elif param[-6:] in ["VelRel","velrel"]:
        # NEED TO RESCALE BY sqrt(a)
        vels = f['SubhaloVel']
        velGroup = f['GroupVel']
        y = vels[:] - velGroup[:]
        yscale = 'linear'
        ylabel = "$v - v_\mathrm{group}\ (km\ s^{-1})$"
    elif param in ["xy","xz","yz"]:
        # trajectory with origin at final position
        pos = f['SubhaloPos']
        dpos = pos - pos[0]
        if param == "xy":
            x = dpos[:,0]
            y = dpos[:,1]
        elif param == "xz":
            x = dpos[:,0]
            y = dpos[:,2]
        else:
            x = dpos[:,1]
            y = dpos[:,2]
        x = FixPeriodic(x,sim) / h
        y = FixPeriodic(y,sim) / h
        if verbose > 0:
            print("pos=",pos[:])
            print("dpos=",dpos)
            print("x = ",x)
        xlabel = '$' + param[0] + "$ (kpc)"
        ylabel = '$' + param[1] + "$ (kpc)"
        yscale = 'linear'
  
    elif isSFR > 0: # sSFR trick
        sfrvarname = param.replace("sSFR","SFR")
        massvarname = param.replace("sSFR","Mass") + "Type"
        massvarname = massvarname.replace("in","In")
        if verbose > 0:
            print("massvarname=",massvarname)
        massStars = f[massvarname]
        massStars = massStars[:,4]*1e10
        y = f[sfrvarname]/massStars # independent of h
        if ylabel is None:
            if usetex:
                ylabel = "$sSFR\ (2\,R_\mathrm{e}) [yr^{-1}]$"
            else:
                ylabel = "sSFR (2 R_e) [1/yr]"           
    else: # use given param
        y = f[param][:]
    if param2 is not None: # particle type if given
        y = y[:,param2]
    if "Mass" in param:
        y = 1e10*y / h
        usetex = False
        ylabel = param + " (solar masses)"
    if verbose > 0:
        print("snapnums=",snapnums)
        print("y = ",y)
        print("ylabel=",ylabel)

    # plot
    if not plot:
        return y

    print("plotting...")
    
    # restrict to chosen snapnums
    if snapmin is None:
        snapmin = snapnums[-1]
    cond = ((snapnums >= snapmin) & (snapnums <= snapmax))
    x = x[cond]
    y = y[cond]

    plt.figure(figsize=[5,5])
    if yscale == 'log':
        ypositive = y[y>0]
        if len(ypositive) == 0:
            raise ValueError("no positive quantities to plot in log scale!")
        if verbose:
            print("len y = ",len(y),"len(ypositive)=",len(ypositive))
        ymin = np.min(ypositive)
        ymin4plot = 10**(np.floor(np.log10(ymin)))
        ymax = np.max(y[y<1e36])
        ymax4plot = 10**(np.ceil(np.log10(ymax)))
#         y2 = np.where(y<=0,ymin4plot,y)
        plt.ylim(0.5*ymin4plot,ymax4plot)
    else:
        y2 = y
    if ylabel is None:
        ylabel = param
        usetex = False
    # limit types to Dark matter, gas, and stars
    print("param = ",param)
    legend = True
    if param in ["SubhaloPos","SubhaloVel","SubhaloSpin","GroupPos","GroupVel",
                 "VelRel"]:
        colors = ('r','g','b')
        labels = ('$x$','$y$','$z$')
    elif param == "SubhaloStellarPhotometrics":
        colors = ('purple','b','darkgreen','gold','g','r','salmon','orange')
        labels = ('$U$','$B$','$V$','$K$','$g$','$r$','$i$','$z$')
    elif ((param.find("Type") > 0) or (param in  ["mass2Re","Mass2Re"])):
        colors = ('g','purple','brown','orange','b','k')
        labels = ('gas','dark matter','type 2','tracers','stars','black holes')
    elif param.find("MetalFractions") > 0:
        colors = ('purple','magenta','brown','orange','g','b','cyan','navy','r','k')
        labels = ('H', 'He', 'C', 'N', 'O', 'Ne', 'Mg', 'Si', 'Fe', 'total')
    else:
        legend = False
    if snapmax-snapmin>12:
        marker = None
    print("ylabel=",ylabel)
    print("labels=",labels)
    t = PlotHistory(y,x,subhaloid=subhaloid,sim=sim,snapmin=snapmin,snapmax=snapmax,
                param=param,param2=param2,yscale=yscale,
                labels=labels,colors=colors,ylabel=ylabel,marker=marker,legend=legend,
                savefig=savefig)
    if param in ["xy","xz","yz"]: 
        return x,y
    else:
        return y, t

def MMP(subhaloid,snapmax=99,snapmin=0,sim='TNG50-1',datarootdir=None,datafileprefix=None,halo=False,verbose=0):
    if datarootdir is None:
        datarootdir = os.getenv("HOME") + "/SIMS/TNG/"
        # answer = input("Enter root directory for TNG simulations: [" + datarootdir + "]")
        # if answer != '':
        #     datarootdir = answer
    treeMethod='sublink_mbp'
    datadir = datarootdir + sim + "/output/"
    if verbose > 0:
        print("History: datadir=",datadir)
    if datafileprefix is None:
        if halo:
            # extract halo URL
            if verbose> 0:
                print("extracting halo URL...")
        else:
            # extract subhalo URL 
            if verbose > 0:
                print("extracting subhalo URL...")
            sub = getsubhalo(subhaloid,simulation=sim,snapnum=snapmax,
                             savedir=datadir,verbose=verbose)
            if verbose > 1:
                print ("sub=",sub)
            progurl = sub['related']['sublink_progenitor']
            if progurl is None:
                print("no progenitor for subhalo", subhaloid)
                return
            # extract tree of main progenitor
            datafile = get(sub['trees'][treeMethod],savedir=datadir)
    elif datafileprefix in [0,'a',"auto"]:
        datafile = datadir + "sublink_mpb_" + str(subhaloid) + ".hdf5"
    else:
        datafile = datadir + datafileprefix + ".hdf5" 
    if verbose > 0:
        print("cwd=",os.getcwd())
        os.system("ls -l " + datafile)
    
    # extract values
    if verbose > 0:
        print("extracting parameters from tree in file " + datarootdir + datafile + "...")
    try:
        f = h5py.File(datafile,'r')
        if verbose > 0:
            print("done")
        
    except:
        # recursive relaunch with datafileprefix=None to force read from TNG database
        print("file ", datafile, " not found on disk, trying TNG database...")
        MMP(subhaloid,snapmax=snapmax,snapmin=snapmin,sim=sim,
            datarootdir=datarootdir,datafileprefix=None,halo=halo,verbose=verbose)


def History(subhaloid,param=None,param2=None,sim='TNG50-1',treeMethod='sublink_mpb',
                snapmin=None,snapmax=99,snapnum=99,datafileprefix=None,plot=True,
                xlabel='snapnum',ylabel=None,yscale='log',ylims=None,relative=True,
                datarootdir=None,halo=False,
                verbose=0,savefig=False,usetex=True,marker=None):
    """Extract and optionally plot evolution of subhalo parameter
    arguments:
        subhaloid
        param=param, where param is one of the subhalo attributes, or shortcuts
            "Mass" (stellar mass within 2 r_h)
            "SFR" (SFR within 2 r_h)
            "sSFR" (sSFR = SFR/Mass_Stars, both within 2 r_h)
        param2=num, where num is the type for Mass (e.g. 4 for Stars)
        snapmax: highest (latest, i.e. base) snapnum (default 99)
        snapmin: lowest (earliest) snapnum (default None last plus one)
        snapnum: reference snapnum for subhaloid (default snapmax)
        datafileprefix: data file prefix (0 or "auto" for sublink_mpb_[subhaloid], None to extract (default))
        halo: if True, assume subhaloID is really haloID [default False]
        plot: do-plot? [boolean]
        xlabel: x-label
        ylabel: y-label
        yscale: scale of y axis
        ylims: [ymin,ymax]
        savefig: output file of figure
        usetex: use TeX fonts?
        marker: marker (default None)
    Author: Gary Mamon (gam AAT iap.fr)"""
    
    # dimensionless Hubble constant
    h = 0.6774
    
    if datarootdir is None:
        datarootdir = os.getenv("HOME") + "/SIMS/TNG/"
        # answer = input("Enter root directory for TNG simulations: [" + datarootdir + "]")
        # if answer != '':
        #     datarootdir = answer
    datadir = datarootdir + sim + "/output/"
    if verbose > 0:
        print("History: datadir=",datadir)
    if datafileprefix is None:
        if halo:
            # extract halo URL
            if verbose> 0:
                print("extracting halo URL...")
        else:
            # extract subhalo URL 
            if verbose > 0:
                print("extracting subhalo URL...")
            if snapnum < snapmax:
                subhaloid = getsubhaloid99(subhaloid,simulation=sim,snapnum=snapnum,verbose=verbose)
                if verbose > 0:
                    print("at snapnum=99, subhaloid = ",subhaloid)
            if verbose > 0:
                print("getsubhalo...")
            sub = getsubhalo(subhaloid,simulation=sim,snapnum=snapmax,
                             savedir=datadir,verbose=verbose)
            if verbose > 1:
                print ("History: sub=",sub)
            progurl = sub['related']['sublink_progenitor']
            if progurl is None:
                print("no progenitor for subhalo", subhaloid)
                return
            # extract tree of main progenitor
            if verbose > 0:
                print("extracting tree of main progenitor with method",treeMethod)
                print("... and saving tree to", datadir)
            datafile = get(sub['trees'][treeMethod],savedir=datadir)
    elif datafileprefix in [0,'a',"auto"]:
        datafile = datadir + "sublink_mpb_" + str(subhaloid) + ".hdf5"
    else:
        datafile = datadir + datafileprefix + ".hdf5" 
    if verbose > 0:
        print("cwd=",os.getcwd())
        os.system("ls -l " + datafile)
    
    # extract valuesass
    if verbose > 0:
        print("extracting parameters from tree in file " + datarootdir + datafile + "...")
    try:
        f = h5py.File(datafile,'r')
        if verbose > 0:
            print("done")
    except:
        # recursive relaunch with datafileprefix=None to force read from TNG database
        print("file ", datafile, " not found on disk, trying TNG database...")
        History(subhaloid,param=param,param2=param2,sim=sim,treeMethod=treeMethod,
                snapmin=snapmin,snapmax=snapmax,snapnum=snapnum,datafileprefix=None,
                plot=plot,
                xlabel=xlabel,ylabel=ylabel,yscale=yscale,ylims=ylims,
                verbose=verbose,savefig=savefig,usetex=usetex,marker=marker)
    if verbose >= 1:
        print("type(f)=",type(f))
        print("f.keys=",f.keys())
    if param is None:
        return f
    snapnums = f['SnapNum'][:]
    x = snapnums.copy()
#     if snapmin is None:
#         if verbose > 0:
#             print("setting snapmin to ",snapnum[-1]-1)
#         snapmin = snapnum[-1]-1
    if verbose > 0:
        print("first 2 SubhaloID = ",f['SubhaloID'][0:2])
        print("first 2 SubfindID = ",f['SubfindID'][0:2])

    # special treatment for specific paramters
    substr = "sSFR"
    isSFR = param.index(substr) if substr in param else -1
    if param in ["sBHmdot"]: # special treatment for specific black hole mass growth rate
        # use 2 R_e aperture
        mdotBH = np.asarray(f['SubhaloBHMdot'])
        massBH= np.asarray(f['SubhaloBHMass'])
        sBHMdot = mdotBH/massBH
        y = np.log10(sBHMdot)
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "\log\,[\dot M_\mathrm{BH}/M_\mathrm{BH}]\ (\mathrm{0.978 Gyr}^{-1})"
            else:
                ylabel = "log [Mdot_BH/M_BH] [1/0.978 Gyr]"
    elif param in ["fgas"]: # special treatment for sSFR like param name
        # use 2 R_e aperture
        massAll = f['SubhaloMassInRadType']
        massStars = massAll[:,4]*1e10
        massGas = massAll[:,0]*1e10
        y = np.log10(massGas/massStars) # independent of h
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "\log\,(M_\mathrm{gas}/M_\mathrm{stars})"
            else:
                ylabel = "log M_gas/M_stars"
    elif param in ["sSFR","ssfr","SSFR"]: # special treatment for sSFR like param name
        # use 2 R_e aperture
        sfr = f['SubhaloSFRinRad']
        massStars = f['SubhaloMassInRadType']
        massStars = massStars[:,4]*1e10
        y = np.log10(sfr/massStars)
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "\log\,[\mathrm{sSFR}\,(2\,R_\mathrm{e})]\ (\mathrm{yr}^{-1})"
            else:
                ylabel = "log sSFR (2 R_e) [1/yr]"
    elif param in ["sfr",'SFR']: # special treatment for SFR like param name
        # use 2 R_e aperture
        sfr = f['SubhaloSFRinRad']
        y = np.log10(sfr[:,])
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "\log\,[\mathrm{SFR}\,(2\,R_\mathrm{e})]\ (\mathrm{M_\odot\,yr}^{-1})"
            else:
                ylabel = "log SFR (2 R_e) [M_sun/yr]"
    elif param in ["mass","Mass"]: # special treatment for mass-like param name
        mass = f['SubhaloMass']
        y = np.log10(1e10 * mass[:] / h)
        yscale = 'linear'
        if ylabel is None:
            if usetex:
                ylabel = "\log\,[\mathrm{mass}_\mathrm{subhalo})]\ (\mathrm{M}_\odot)"
            else:
                ylabel = "log mass (subhalo) [M_sun]"
    elif param in ["mass2Re","Mass2Re"]: # special treatment for mass like param name
        mass = f['SubhaloMassInRadType']
        y = np.log10(1e10 * mass[:,:] / h)
        yscale = 'linear'
        param2 = (0,1,4,5)
        if ylabel is None:
            if usetex:
                ylabel = "\log\,[\mathrm{mass}\ (2\,R_\mathrm{e})]\ (\mathrm{M}_\odot)"
            else:
                ylabel = "log mass (2 R_e) [M_sun]"
    elif param in ["Group_M_Crit200","Group_R_Crit200",
                  "Group_M_Crit500","Group_R_Crit500"]:
        ytmp = f[param]
        if param[6] == 'M':
            y = np.log10(1e10 * ytmp[:] / h)
        else:
            y = ytmp
        yscale = 'linear'
        ylabel = param.replace('Crit200','\mathrm{200,c}')
        ylabel = ylabel.replace("Crit500","\mathrm{500,c}")
        if param[6] == 'M':
            ylabel = ylabel + ' (\mathrm{M}_\odot)'
        else:
            ylabel = ylabel + ' (kpc)'
        ylabel = ylabel.replace("Group_","")
    elif param in ["r_over_R_Crit200","r_over_R_Crit500"]:
        paramRef = "Group_" + param[7:]
        yRef = f[paramRef]
        pos = f['SubhaloPos']
        posGroup = f['GroupPos']
        y = np.zeros(len(pos[:]))
        if verbose > 0:
            print("pos[:,:] = ",pos[:,:]) 
            print("posGroup[:,:]=",posGroup[:,:])
            print("yRef = ", yRef[:])
        # r^2 = dx^2 + dy^2 + dz^2
        for j in range(3): # loop over cartesian axes
            dr = FixPeriodic(pos[:,j]-posGroup[:,j],sim)
            y = y + dr*dr
        # r / R_vir (independent of h)
        y = np.sqrt(y) / yRef
        yscale = 'log'
        ysuf = param[7:].replace('Crit200','\mathrm{200,c}')
        ysuf = ysuf.replace("Crit500","\mathrm{500,c}")
        ylabel = "$r/" + ysuf + "$"
    elif param[-3:] in ["Pos","pos"]:
        pos = f[param]
        if relative:
            y = pos[:]-pos[0,:]
        else:
            y = pos[:]
        # handle periodic boundary conditions
        y = FixPeriodic(y,sim) / h
        yscale = 'linear'
        ylabel = "$\mathrm{" + param + " - " + param + "}$[99] (kpc)"
    elif param[-3:] in ["PosRel","posrel"]:
        # show positions relative to group
        pos = f[param]
        posGroup = f['GroupPos']
        y = pos - posGroup
        # watch for crossing of box boundaries
        y = FixPeriodic(y, sim) / h
        yscale = 'linear'
        ylabel = "$\mathrm{" + param + " - " + param + "_\mathrm{group}}$ (kpc)"
    elif param[-3:] in ["Vel","vel"]:
        # NEED TO RESCALE BY sqrt(a)
        vels = f[param]
        y = vels[:]
        yscale = 'linear'
        ylabel = '$\mathrm{' + param + "\ (km\ s^{-1})}$"
    elif param[-6:] in ["VelRel","velrel"]:
        # NEED TO RESCALE BY sqrt(a)
        vels = f['SubhaloVel']
        velGroup = f['GroupVel']
        y = vels[:] - velGroup[:]
        yscale = 'linear'
        ylabel = "$v - v_\mathrm{group}\ (km\ s^{-1})$"
    elif param in ["xy","xz","yz"]:
        # trajectory with origin at final position
        pos = f['SubhaloPos']
        print("type(pos)=",type(pos))
        print("type(pos[:,0])=",type(pos[:,0]))
        if relative:
            dpos = (pos - pos[0])
        else:
            dpos = np.zeros((len(pos),3))
            for i in range(3):
                dpos[:,i] = pos[:,i]/1000 # in Mpc/h
        if param == "xy":
            x = dpos[:,0]
            y = dpos[:,1]
        elif param == "xz":
            x = dpos[:,0]
            y = dpos[:,2]
        else:
            x = dpos[:,1]
            y = dpos[:,2]
        x = FixPeriodic(x,sim) / h
        y = FixPeriodic(y,sim) / h
        if verbose > 0:
            print("pos=",pos[:])
            print("dpos=",dpos)
            print("x = ",x)
        if relative:
            xlabel = '$' + param[0] + "$ (kpc)"
            ylabel = '$' + param[1] + "$ (kpc)"
        else:
            xlabel = '$' + param[0] + "$ (Mpc)"
            ylabel = '$' + param[1] + "$ (Mpc)"
        yscale = 'linear'
    elif param in ["EJ","EL","Energy-AngMom"]: # energy and abular momentum both per unit mass
        pos = f['SubhaloPos']
        posGroup = f['GroupPos']
        rvir = f['Group_R_Crit200']
        G = 43
        Delta = 200
        H = h*cu.Eofz(Omegam0,1-Omegam0,z)
        vvir = 100 * np.sqrt(Delta/2) * H * rvir
        r = np.zeros(len(pos[:]))
        # r^2 = dx^2 + dy^2 + dz^2
        for j in range(3): # loop over cartesian axes
            dr = FixPeriodic(pos[:,j]-posGroup[:,j],sim)
            r = r + dr*dr
        r = np.sqrt(r)
        pot = vvir * cu.phiNFWhat(r/rvir)
    elif isSFR > 0: # sSFR trick
        sfrvarname = param.replace("sSFR","SFR")
        massvarname = param.replace("sSFR","Mass") + "Type"
        massvarname = massvarname.replace("in","In")
        if verbose > 0:
            print("massvarname=",massvarname)
        massStars = f[massvarname]
        massStars = massStars[:,4]*1e10
        y = f[sfrvarname]/massStars # independent of h
        if ylabel is None:
            if usetex:
                ylabel = "sSFR\ (2\,R_\mathrm{e}) [yr^{-1}]"
            else:
                ylabel = "sSFR (2 R_e) [1/yr]"           
    else: # use given param
        y = f[param][:]
    if param2 is not None: # particle type if given
        y = y[:,param2]
    if "Mass" in param:
        # y = 1e10*y / h
        usetex = False
        ylabel = param + " (solar masses)"
    if verbose > 0:
        print("snapnums=",snapnums)
        print("y = ",y)
        print("ylabel=",ylabel)

    # plot
    if not plot:
        return y

    print("plotting...")
    
    # restrict to chosen snapnums
    if snapmin is None:
        snapmin = snapnums[-1]-1
    cond = ((snapnums >= snapmin) & (snapnums <= snapmax))
    x = x[cond]
    y = y[cond]

    plt.figure(figsize=[5,5])
    if yscale == 'log':
        ypositive = y[y>0]
        if len(ypositive) == 0:
            raise ValueError("no positive quantities to plot in log scale!")
        if verbose:
            print("len y = ",len(y),"len(ypositive)=",len(ypositive))
        ymin = np.min(ypositive)
        ymin4plot = 10**(np.floor(np.log10(ymin)))
        ymax = np.max(y[y<1e36])
        ymax4plot = 10**(np.ceil(np.log10(ymax)))
#         y2 = np.where(y<=0,ymin4plot,y)
        plt.ylim(0.5*ymin4plot,ymax4plot)
    else:
        y2 = y
    if ylabel is None:
        ylabel = param
        usetex = False
    # limit types to Dark matter, gas, and stars
    if verbose > 0:
        print("param = ",param)
    legend = True
    if param in ["SubhaloPos","SubhaloVel","SubhaloSpin","GroupPos","GroupVel",
                 "VelRel"]:
        colors = ('r','g','b')
        labels = ('$x$','$y$','$z$')
    elif param == "SubhaloStellarPhotometrics":
        colors = ('purple','b','darkgreen','gold','g','r','salmon','orange')
        labels = ('$U$','$B$','$V$','$K$','$g$','$r$','$i$','$z$')
    elif ((param.find("Type") > 0) or (param in  ["mass2Re","Mass2Re"])):
        colors = ('g','purple','brown','orange','b','k')
        labels = ('gas','dark matter','type 2','tracers','stars','black holes')
    elif param.find("MetalFractions") > 0:
        colors = ('purple','magenta','brown','orange','g','b','cyan','navy','r','k')
        labels = ('H', 'He', 'C', 'N', 'O', 'Ne', 'Mg', 'Si', 'Fe', 'total')
    else:
        legend = False
    if snapmax-snapmin>12:
        marker=None
    if legend:
        if param2 is not None:
            if isinstance(param2,Iterable): # param2 is a list or tuple or array
                for i, par2 in enumerate(param2):
                    plt.plot(x,y[:,i],label=labels[par2],
                             color=colors[par2],marker=marker)
            else: # param2 is a scalar
                par2 = param2
                plt.plot(x,y[:],color=colors[par2],marker=marker)
                legend = False
        else: # param2 is not given
            for i in range(len(labels)):
                if ((param.find("Type") > 0) & (i in [2,3])):
                    continue
                plt.plot(x,y[:,i],label=labels[i],
                         color=colors[i],marker=marker)
        # check again legend, which may have been turned off in the block above
        if legend:
            if ((len(labels) <= 4) or (param.find("Type") > 0)):
                fsize = 16
            else:
                fsize = int(32/np.sqrt(len(labels)))
            print("len(labels)=",len(labels))
            print("legend fontsize = ",fsize)
#             if param == "SubhaloStellarPhotometrics":
#                 print("small legend?")
#                 plt.legend(fontsize="small")
#             else:
            plt.legend(fontsize=fsize)
    elif param in ["xy","xz","yz"]:
        plt.plot(x,y,marker=marker)
        plt.scatter([x[0]],[y[0]],marker='o',c='k',s=150)
        plt.text(x[0],y[0],"99",c='orange',fontsize=10,
                 horizontalalignment='center',verticalalignment='center')
    else: # Type is not in parameter name
        plt.plot(x,y,marker=marker)
        if param in ["mass","Mass"]:
            plt.legend()
    if usetex: # assume LaTeX labels
        # check that the label has no $ signs
        if ((xlabel[0] != '$') & (xlabel[-1] != '$')):
            xlabel = '$\mathrm{' + xlabel + '}$'
        if ((ylabel[0] != '$') & (ylabel[-1] != '$')):
            ylabel = '$' + ylabel + '$'
    if verbose > 0:
        print("ylabel=",ylabel)
    plt.xlabel(xlabel, fontsize=18)
    plt.ylabel(ylabel, fontsize=18)
    plt.yscale(yscale)
    if snapmax is None:
        snapmax = 99
    if param not in ["xy","xz","yz"]: 
        plt.xlim(snapmin,snapmax+1)
    if ylims is not None:
        plt.ylim(ylims)    
    elif param == "SubhaloStellarPhotometrics":
        if np.max(y) > -1:
            ygood = y[(y>-30) & (y<-1)]
            plt.ylim(np.min(ygood)-0.5,np.max(ygood)+0.5)
#     plt.tight_layout(w_pad=0.5, h_pad=1)
#     plt.grid()
    plt.title(sim + "  subhalo " + str(subhaloid), fontsize=18)
    if savefig is not False: # save figure to PDF file
        if snapmin > 1:
            snapsuf = str(snapmin) + "to" + str(snapmax)
        else:
            snapsuf = ""
        if savefig in [0,'auto']: # automatic file prefix
            if param in ["xy","xz","yz"]:
                savefig = param + "_" + sim + str(subhaloid) + "_" + snapsuf
            else:
                savefig = param + "vsSnapnum_" + sim + str(subhaloid) + "_" + snapsuf
        print("plotting into file",savefig + ".pdf")
#         plt.savefig(savefig + ".pdf",bbox_inches='tight',pad_inches=0.25)
#        savefigGAM(savefig + "_GAM.pdf")
        plt.tight_layout()
        plt.savefig(savefig + ".pdf",bbox_inches='tight')
    if param in ["xy","xz","yz"]: 
        return x,y
    else:
        return y
    
def EnergyAngmomHistory(subhaloid,sim='TNG50-1',snapnum=99,snapmin=0,snapmax=99,
                        treeMethod='sublink_mpb',datarootdir=None,datafileprefix=None, 
                        verbose=0):
    if datarootdir is None:
        datarootdir = os.getenv("HOME") + "/SIMS/TNG/"
        # answer = input("Enter root directory for TNG simulations: [" + datarootdir + "]")
        # if answer != '':
        #     datarootdir = answer
    datadir = datarootdir + sim + "/output/"
    if verbose > 0:
        print("History: datadir=",datadir)
    if datafileprefix is None:
        # extract subhalo URL 
        if verbose > 0:
            print("extracting subhalo URL...")
        if snapnum < snapmax:
            subhaloid = getsubhaloid99(subhaloid,simulation=sim,snapnum=snapnum,verbose=verbose)
            if verbose > 0:
                print("at snapnum=99, subhaloid = ",subhaloid)
        if verbose > 0:
            print("getsubhalo...")
        sub = getsubhalo(subhaloid,simulation=sim,snapnum=snapmax,
                         savedir=datadir,verbose=verbose)
        if verbose > 0:
            print ("History: sub=",sub)
        progurl = sub['related']['sublink_progenitor']
        if progurl is None:
            print("no progenitor for subhalo", subhaloid)
            return
        # extract tree of main progenitor
        if verbose > 0:
            print("extracting tree of main progenitor with method",treeMethod)
            print("... and saving tree to", datadir)
        datafile = get(sub['trees'][treeMethod],savedir=datadir)
    elif datafileprefix in [0,'a',"auto"]:
        datafile = datadir + "sublink_mpb_" + str(subhaloid) + ".hdf5"
    else:
        datafile = datadir + datafileprefix + ".hdf5" 
    if verbose > 0:
        print("cwd=",os.getcwd())
        os.system("ls -l " + datafile)
    
    # extract values
    if verbose > 0:
        print("extracting parameters from tree in file " + datarootdir + datafile + "...")
    try:
        f = h5py.File(datafile,'r')
    except:
        raise ValueError("cannot open file " + datafile)

    pos = f['SubhaloPos']
    posGroup = f['GroupPos']
    rvir = f['Group_R_Crit200']
    vels = f['SubhaloVel']
    velsGroup = f['GroupVel']
    snapnums = f['SnapNum']
    # print("snapnums from f = ", snapnums[:])
    # print("f.keys = ",f.keys())
    Delta = 200
    # snapnums = np.arange(snapmin,snapmax+1)
    z = zofsnap(snapnums)
    # print("z = ",z)
    hofz = h * Eofz(Omegam0,1-Omegam0,z)
    # print("hofz=",hofz)
    vvir = 100 * np.sqrt(Delta/2) * (hofz/1000) * rvir # in km/s
    r = np.zeros(len(pos[:]))
    dpos = np.zeros((len(pos),3))
    dvels = np.zeros((len(vels),3))
    # r^2 = dx^2 + dy^2 + dz^2
    for j in range(3): # loop over cartesian axes
        dpos[:,j] = FixPeriodic(pos[:,j]-posGroup[:,j],sim)
        dvels[:,j] = vels[:,j] - velsGroup[:,j]
    r = np.sqrt(np.sum(dpos[:,:]*dpos[:,:],axis=1))
    # r = np.sqrt(r)
    r_over_rvir = r/rvir
    
    # potential (assuming c=4 NFW for now)
    c = 6
    phihat0 = -1/(np.log(1+c)-c/(1+c))
    # print("phihat0=",phihat0)
    phihat = phihat0 * np.log(1+c*r_over_rvir)/r_over_rvir
    # print("phihat=",phihat)
    # print("rvir = ",rvir[:])
    # print("vvir = ",vvir)
    pot = vvir**2 * phihat
    # print("min max pot = ",np.min(pot),np.max(pot))
    # print("pot = ",pot)
    # kinetic term
    # vsq = vels[:,0]*vels[:,0] + vels[:,1]*vels[:,1] + vels[:,2]*vels[:,2]
    vsq = np.sum(dvels[:,:]*dvels[:,:],axis=1)
    # energy per unit mass
    # print("kin = ",0.5*vsq)
    E = 0.5*vsq + pot # in (km/s)^2
    
    # angular momentum

    # print("r=",r)
    vr = np.sum(dpos[:,:]*dvels[:,:],axis=1)/r
    # print("dpos=",dpos[:,:])
    # print("dvels=",dvels[:,:])
    # print("vsq=",vsq)
    # print("vr = ",vr)
    vt = np.sqrt(vsq-vr*vr)
    # print("vt=",vt)
    J = r*vt   # in kpc*(km/s)
    
    # print("type(snapnums) = ",type(snapnums))
    # print("snapnums=",snapnums,"min max =",snapnums.min(),snapnums.max())
    # print("rvir=",rvir)
    # print("snapmin=",snapmin,"snapmax=",snapmax)
    # snapnums = np.arange(0,100)
    cond = ((snapnums[:] >= snapmin) & (snapnums[:] <= snapmax))
    snaps = snapnums[cond]
    E = E[cond]
    J = J[cond]
    plt.scatter(snaps,-E,label='-E (km/s)$^2$')
    plt.scatter(snaps,J,label='J (kpc$\,$km/s)')
    plt.yscale('log')
    plt.legend()
    
    return E, J, dpos, dvels

def FitMainSequence(subhalodict,lssfrThreshold=-12,maxlMass=10,plot=False):
    lmass,lsfr = 10+np.log10(subhalodict['SubhaloMassInRadType'][:,4]),np.log10(subhalodict['SubhaloSFRinRad'])
    lssfr = lsfr - lmass
    lmbins = np.linspace(8,12,9)
    dlm = lmbins[1]-lmbins[0]
    lmbins = lmbins + dlm/2 # so that first bin is from 8 to 8.5
    lssfrMed = np.zeros(len(lmbins))
    lssfrMean = np.zeros(len(lmbins))
    # sfrThreshold = 10**lsfr_Threshold
    for i, lmb in enumerate(lmbins):
        # select by mass and above SFR threshold 
        cond = ((np.abs(lmass-lmb)<dlm/2) & (lssfr > lssfrThreshold))
        lmasstmp,lssfrtmp = lmass[cond],lssfr[cond]
        lssfrMed[i] = np.median(lssfrtmp)
        lssfrMean[i] = lssfrtmp.mean()
    lssfrMode = 3*lssfrMed - 2*lssfrMean
    
    # fit line
    
    coeffsMedian = np.polyfit(lmbins[lmbins<maxlMass],lssfrMed[lmbins<maxlMass],1)
    coeffsMode = np.polyfit(lmbins[lmbins<maxlMass],lssfrMode[lmbins<maxlMass],1)
    
    if plot:
        plt.scatter(lmass,lssfr,s=1)
        plt.scatter(lmbins,lssfrMed,label='median')
        plt.scatter(lmbins,lssfrMode,label='mode')
        plt.plot(lmbins,lmbins*coeffsMedian[0]+coeffsMedian[1],label='median')
        plt.plot(lmbins,lmbins*coeffsMode[0]+coeffsMode[1],label='mode')
        plt.xlim(8,12)
        plt.ylim(-13)
        plt.xlabel('$\log\,(M_\star/\mathrm{M}_\odot)$')
        plt.ylabel('$\log\,(\mathrm{sSFR}/\mathrm{yr}^{-1})$')
        plt.legend()
        plt.savefig(plot + '.pdf')
        
    return coeffsMedian, coeffsMode

def FitMainSequence2(subhalodict,lssfrThreshold=-12,maxlMass=10,minlMass=8,plot=False):
    lmass,lsfr = (10+np.log10(subhalodict['SubhaloMassInRadType'][:,4]),
                    np.log10(subhalodict['SubhaloSFRinRad']))
    lssfr = lsfr - lmass
    lmbins = np.linspace(8,12,9)
    dlm = lmbins[1]-lmbins[0]
    lmbins = lmbins + dlm/2 # so that first bin is from 8 to 8.5
    lssfrMed = np.zeros(len(lmbins))
    lssfrMean = np.zeros(len(lmbins))
    # sfrThreshold = 10**lsfr_Threshold
    for i, lmb in enumerate(lmbins):
        # select by mass and above SFR threshold 
        cond = ((np.abs(lmass-lmb)<dlm/2) & (lssfr > lssfrThreshold))
        lmasstmp,lssfrtmp = lmass[cond],lssfr[cond]
        lssfrMed[i] = np.median(lssfrtmp)
        lssfrMean[i] = lssfrtmp.mean()
    lssfrMode = 3*lssfrMed - 2*lssfrMean
    
    # fit line
    
    coeffsMedian = np.polyfit(lmbins[lmbins<maxlMass],lssfrMed[lmbins<maxlMass],1)
    coeffsMode = np.polyfit(lmbins[lmbins<maxlMass],lssfrMode[lmbins<maxlMass],1)

    # iterate up to 3 times
    
    for j in range(3):
        print("iteration",j+1)
        # residuals

        dlssfr = lssfr - (coeffsMode[0]*lmass + coeffsMode[1])
        lmbins2 = np.linspace(8,maxlMass,int(1+(maxlMass-8)*2))
        sigResiduals = np.zeros(len(lmbins2))
        for i, lmb in enumerate(lmbins2):
            # select by mass 
            cond = ((np.abs(lmass-lmb)<dlm/2) & (dlssfr > -10))
            dlssfrtmp = dlssfr[cond]
            sigResiduals[i] = np.std(dlssfrtmp)

        # fit line to the standard deviation of the residuals 

        print("lmbins2 = ",lmbins2)
        print("sigResiduals=",sigResiduals)
        plt.scatter(lmbins2,sigResiduals)
        coeffsSTD = np.polyfit(lmbins2,sigResiduals,1)

        # remove points that are beyond 3 sigma

#         cond = np.abs(lssfr - lssfrMode) < 3*(coeffsSTD[0]*lmass+coeffsSTD[1])

#         lmass = lmass[cond]
#         lssfr = lssfr[cond]
        lssfrMed = np.zeros(len(lmbins))
        lssfrMean = np.zeros(len(lmbins))
        # sfrThreshold = 10**lsfr_Threshold
        for i, lmb in enumerate(lmbins):
            # select by mass and above SFR threshold 
            cond = ((np.abs(lmass-lmb)<dlm/2) & (np.abs(lssfr - lssfrMode[i]) < 3*(coeffsSTD[0]*lmass+coeffsSTD[1])))
            lmasstmp,lssfrtmp = lmass[cond],lssfr[cond]
            print("lmass=",lmb,"N=",len(lmasstmp))
            lssfrMed[i] = np.median(lssfrtmp)
            lssfrMean[i] = lssfrtmp.mean()
        lssfrMode = 3*lssfrMed - 2*lssfrMean

        # fit line

        coeffsMedian = np.polyfit(lmbins[lmbins<maxlMass],lssfrMed[lmbins<maxlMass],1)
        coeffsMode = np.polyfit(lmbins[lmbins<maxlMass],lssfrMode[lmbins<maxlMass],1)
        print("Mode: slope=",coeffsMode[0],"value at 10=",coeffsMode[0]*10+coeffsMode[1])
        
    if plot:
        print("plotting...")
        plt.scatter(lmass,lssfr,s=1)
        plt.scatter(lmbins,lssfrMed,label='median')
        plt.scatter(lmbins,lssfrMode,label='mode')
        plt.plot(lmbins,lmbins*coeffsMedian[0]+coeffsMedian[1],label='median')
        plt.plot(lmbins,lmbins*coeffsMode[0]+coeffsMode[1],label='mode')
        plt.xlim(8,12)
        plt.ylim(-13)
        plt.xlabel('$\log\,(M_\star/\mathrm{M}_\odot)$')
        plt.ylabel('$\log\,(\mathrm{sSFR}/\mathrm{yr}^{-1})$')
        plt.legend()
        if plot == 'auto':
            plt.show()
        else:
            plt.savefig(plot + '.pdf')        
    return coeffsMode

def partTemperature(f):
    """
    temperature given internal energy from U = kT/[(gamma-1)mu m_p]
    where mu is mean particle mass in proton masses (code uses grams)
    
    argument: h5py file object
    source: https://www.tng-project.org/data/docs/faq/
    """
    u           = f['PartType0']['InternalEnergy'][:]    #  Internal Energy
    # VelDisp     = f['PartType0']['SubfindVelDisp'][:]
    X_e         = f['PartType0']['ElectronAbundance'][:] # electron abundance
    X_H         = 0.76             # Hydrogen mass fraction
    gamma       = 5.0/3.0          # adiabatic index
    kB          = 1.3807e-16       # Boltzmann constant in CGS units  [cm^2 g s^-2 K^-1]
    kB_kev      = 8.6173324e-8 
    mp          = 1.6726e-24       # proton mass  [g]
    mu          = (4*mp) / (1 + 3*X_H + 4*X_H*X_e) # mean particle mass
    temperature = (gamma-1)* (u/kB)* mu * 1e10
    return temperature

def testParticles():
    savedir = homedir + "/NOSAVE/SIMS/TNG/"
    # saveParticles(300907,savedir=savedir,request={'stars':'Masses,GFM_InitialMass,GFM_StellarFormationTime'})
    # toto = particles(300907,PartType=4,params="Masses",verbose=1)
    data = particles(300906,PartType=4,params=["Masses","GFM_InitialMass","GFM_StellarFormationTime"],verbose=1,
                     extract=True,savedir=savedir)
    print("type data[0] = ",type(data[0]))
    plt.scatter(10+np.log10(data[0]),10+np.log10(data[1]),s=1)
    lm = np.linspace(4, 5.5, 21)
    plt.plot(lm,lm,c='r')
    plt.xlabel('particle mass ($\mathrm{M}_\odot$)')
    plt.ylabel('particle initial mass ($\mathrm{M}_\odot$)')

def H0inverseinGyr(h):
    """inverse Hubble consytant in Gyr
    arg: h = H_0/(100km/s/Mpc)
    author: Gary Mamon"""
    yearindays = 365.25
    dayinseconds = 86400
    yearinseconds = yearindays * dayinseconds
    Mpc = 3.0857e24 # in cm
    return 1e-9/yearinseconds / (h*100*1e5/Mpc)

def Eofz(Omegam,Omegal,z,Omegar=0):
    """Dimensionless Hubble constant H(z)/H_0
    args:
        Omegam: z=0 density parameter
        Omegal: z=0 dark energy parameter
        z:      redshift
        Omegar: z=0 radiaton density parameter
    author: Gary Mamon"""
    # need to generalize for z as either scalar or numpy array
    return np.sqrt(Omegam*(1+z)**3 + Omegal + (1-Omegam-Omegal)*(1+z)**2 + Omegar*(1+z)**4)

def Omegam(Omegam0,Omega_lambda0,z,Omegar0=0):
    """Density parameter for given redshift
    author: Gary Mamon"""
    return Omegam0*(1+z)**3 / Eofz(Omegam0,Omega_lambda0,z,Omegar0) ** 2

def H0tflat(Omegam0,z):
    """Dimensionless time in flat Universe
    args:
        Omegam0:     Omega_matter,0
        z:          redshift
    source: Carroll, Press & Turner 92, ARAA, eq. (17)
    author Gary Mamon"""
     # need to generalize for z as either scalar or numpy array
    Om = Omegam(Omegam0,1-Omegam0,z)
    return 2/3*np.arcsinh(np.sqrt((1-Om)/Om)) / np.sqrt(1-Om)/Eofz(Omegam0,1-Omegam0,z)

def AgeUniverse(Omegam0,h,z=0):
    """Age of Universe for given redshift
    args:
        Omegam0: density parameter at z=0
        h:       H_0/(100 km/s/Mpc)
        z:       redshift
    author: Gary Mamon"""
    return H0tflat(Omegam0,z) * H0inverseinGyr(h)

def get_z_t(snapmin,snapmax=99,sim='TNG50-1'):
    # snaps = range(snapmin,snapmax+1)
    snap_z = snapofz('all')
    z = np.array([sz[1] for sz in snap_z if sz[0] >= snapmin and sz[0] <= snapmax])
    t = AgeUniverse(Omega_m, h, z)
    return z, t

def get_sats(group_id_zmin,snap_min=50,snap_max=99,sim='TNG50-1',basePath=None,
             MinLogSatMass=None,
             halos_zmin=None,halos_z=None,verbose=0):
    """
    Satellites of group that is not merged into more massive group later on
    Arguments:
        group_id_zmin : group_id at z=z_min
        snap_min: snapnum at z = z_max (starting time, default 50 [z=1])
        snap_max: snapnum at z = z_min (ending time, default 99 [z=0])
        sim : simulation (default: 'TNG50-1')
        MinLogSatMass : min log stellar mass at start redshift 
            (default None, auto 7.5 and 8.5 for TNG50 and TNG100)
        halos_zmin : Illustris dictionary of all halos at z=0 (default None)
        halos_z : Illustris dictionary of all halos at start redshift (default None)
        verbose : verbosity (default 0)

    Returns:
        satellite IDs at starting redshift
        
    Authors:
        Gary Mamon & Houda Haidar
    """
    
    # initialization
    if MinLogSatMass is None:
        if sim == "TNG50-1":
            MinLogSatMass = 7.5
        elif sim == "TNG100-1":
            MinLogSatMass = 8.5
    if basePath is None:
        basePath = os.getenv("HOME") + "/SIMS/TNG/" + sim + "/output"
        
    # all centrals of z=z_min groups
    if halos_zmin is None:
        print("extracting halos...")
        halos_zmin = il.groupcat.loadHalos(basePath=basePath, snapNum=99)
    centrals = halos_zmin['GroupFirstSub']
    
    # central of selected z=z_min group
    subcen_zmin = centrals[group_id_zmin]
    if verbose > 0:
        print("Histories for central subhalo at z_min: ",subcen_zmin)
        
    # most massive progenitors at z
    ff = History(subcen_zmin,sim=sim,verbose=verbose)
    # mmps = ff['SubfindID'][:]
    mmps = ff['SubhaloID'][:]
    groups = ff['SubhaloGrNr'][:]
    # mmps = History(subcen_zmin,param='SubfindID',plot=False)
    # groups = History(subcen_zmin,param='SubhaloGrNr',plot=False)
    snapnums = np.linspace(99,99-len(groups)+1,len(groups))
    subcen_z = mmps[snapnums==snap_min]
    group_z = groups[snapnums==snap_min]
    if halos_z is None:
        print("re-extacting halos...")
        halos_z = il.groupcat.loadHalos(basePath=basePath, snapNum=snap_min)
        print("done")
    GroupFirstSub_z = halos_z['GroupFirstSub'][group_z]
    if verbose > 0:
        print("subcen_z GroupFirstSub_z type = ",subcen_z,GroupFirstSub_z,type(subcen_z))
    
    # satellites of same group at z
    GroupNsubs_z = halos_z['GroupNsubs'][group_z]
    if GroupNsubs_z == 1:
        if verbose > -1:
            print("no satellites!")
        return np.empty(1)
    
    satellites = np.arange(GroupFirstSub_z+1, GroupFirstSub_z+GroupNsubs_z)
    
    # stellar masses and flags
    if verbose > 0:
        print("basePath=",basePath)
    tab = il.groupcat.loadSubhalos(basePath=basePath,snapNum=snap_min,
                                   fields=['SubhaloMassInRadType','SubhaloFlag'])
    sat_masses_z = tab['SubhaloMassInRadType'][satellites][:,4]
    sat_flags_z = tab['SubhaloFlag'][satellites]
    
    # filter on stellar masses and flags
    cond = (sat_flags_z == 1) & (sat_masses_z*1e10 > 10**MinLogSatMass)
    return satellites[cond]

def TraceBack(group_id_snap_max,snap_max=99,snap_start=50,simulation='TNG50-1',
              groups_snap_max=None,groups_snap_start=None,extract=False,verbose=0):
    
    # 1) All groups at snapnum=snap_max and snapnum=snap_start (slow, do once)
    basePath = home + "/SIMS/TNG/" + simulation + "/output"
    if groups_snap_max is None:
        if verbose > 0:
            print("extracting halos from disk for snapnum=",snap_max,"...")
        groups_snap_max = il.groupcat.loadHalos(basePath=basePath, snapNum=snap_max)

    if groups_snap_start is None:
        if verbose > 0:
            print("extracting halos from disk for snapnum=",snap_start,"...")
        groups_snap_start = il.groupcat.loadHalos(basePath=basePath, snapNum=snap_start)
    
    # 2) All central subhalos at that snapnum
    centrals = groups_snap_max['GroupFirstSub']
    
    # 3) central of selected z=z_min group
    subcen_snap_max = centrals[group_id_snap_max]
    
    # 4) most massive progenitor of that central to z (using sublink_mpb) —>  f
    if verbose > 0:
        print("subcen_snap_max=",subcen_snap_max)
    f = getMPB(subcen_snap_max,sim=simulation,verbose=verbose)
    groups = f['SubhaloGrNr'][:]
    snaps = f['SnapNum'][:]
    group_snap_start = groups[snaps==snap_start][0]
    if verbose > 0:
        print("group_snap_start=",group_snap_start)
    group_dict = gethalo(group_snap_start,snapnum=snap_start,simulation=simulation)
    subcen_snap_start = group_dict['GroupFirstSub']
    Nsubs_snap_start = group_dict['GroupNsubs']
    if Nsubs_snap_start == 1:
        print("no satellites!")
        return -1
    subsats_snap_start = np.arange(subcen_snap_start+1, subcen_snap_start+ Nsubs_snap_start)
    
    # 5) trace forward history of descendants (and possible other parameters)
    f = []
    for i, subsat in enumerate(subsats_snap_start):
        f.append(getMDB(subsat,snapnum=snap_start,sim=simulation,verbose=verbose))
    return f, groups_snap_max, groups_snap_start

    # 6) NEED TO FIND WHEN subhalo IS MERGED INTO MORE MASSIVE ONE
    # USING DesctructionNext FORWARD IN THE TREE
    
def MainProg(subhalo,snap=99,sim='TNG50-1',method='url',wSnap=False,verbose=0):
    """Main progenitor subhalo
    author: Gary Mamon (gam AAT iap.fr) & Houda Haidar
    """
    if method == "url":
        url = baseUrl + sim + "/snapshots/" + str(snap) + "/subhalos/" + str(subhalo) + "/"
        f = get(url)
        if wSnap:
            return f['prog_sfid'],f['prog_snap']
        else:
            return f['prog_sfid']
    # elif method == "getMPB":
    #     f = getMPB(subhalo,snap,sim=sim,verbose=verbose)
    #     return f['SubfindID'][1]
    else:
        raise ValueError("cannot understand method=" + method)
    
def MainDesc(subhalo,snap=50,sim='TNG50-1',method='url',wSnap=False,verbose=0):
    """Main descendant subhalo
    author: Gary Mamon (gam AAT iap.fr) & Houda Haidar
    """
    if method == "url":
        url = baseUrl + sim + "/snapshots/" + str(snap) + "/subhalos/" + str(subhalo) + "/"
        f = get(url)
        if wSnap:
            return f['desc_sfid'],f['desc_snap']
        else:
            return f['desc_sfid']  
    # elif method == "getMDB":
    #     f = getMDB(subhalo,snap,sim=sim,verbose=verbose)
    #     return f['SubfindID'][1]
    else:
        raise ValueError("cannot understand method=" + method)
        
def DestructionNext(subhalo,snap=50,sim='TNG50-1',verbose=0):
    """Will subhalo be merged into more massive one on next step?
    returns boolean
    author: Gary Mamon (gam AAT iap.fr) & Houda Haidar"""
    # test that main progenitor of main descendant is not subhalo itself
    cond = MainProg(MainDesc(subhalo,snap=snap,sim=sim),snap=snap+1,sim=sim) != subhalo
    return cond

def GroupMergeNext(subhalo,snap=50,sim='TNG50-1',verbose=0):
    """Will halo be merged into more massive one on next step?
    returns boolean
    author: Gary Mamon (gam AAT iap.fr) & Houda Haidar"""
    cen = Central(subhalo,snap=snap,sim=sim)
    cond = MainProg(MainDesc(cen,snap=snap,sim=sim),snap=snap+1,sim=sim) != cen
    return cond

def Central(subhalo,snap=50,sim='TNG50-1'):
    return gethalo(subhalo,subhalo=True,snapnum=snap)['GroupFirstSub']

def DestructionNextHouda(subhalo,snap=50,sim='TNG50-1',verbose=0):
    """Will subhalo be merged into more massive one on next step?
    returns boolean
    False : means yes, it will merge
    True  : means no, it won't merge
    author: Houda Haidar"""
    # test that main progenitor of main descendant is not subhalo itself
    desc, desc_snap = MainDesc(subhalo,snap=snap,sim=sim,wSnap=True)
    if verbose > 1:
        print("desc,",desc,"desc_snap",desc_snap)
    #print("descendant")
    #print(desc, desc_snap)
    prog, prog_snap = MainProg(desc,snap=snap+1,sim=sim,wSnap=True)
    #print("progenitor")
    #print(prog, prog_snap)
    cond = (prog != subhalo)
    if verbose > 1:
        print("(prog != subhalo)?= ",cond)
    return desc, desc_snap, cond

def LastDescBeforeMerge(subhalo,snap=50,sim='TNG50-1',verbose=0):
    merge = False
    while snap < 99:
        if verbose > 0:
            print("snap subhalo = ", snap,subhalo)
        subhalo_old = subhalo
        snap_old = snap
        subhalo, snap, cond = DestructionNextHouda(subhalo,snap,sim=sim)
        if verbose > 1:
            print("now snap subhalo = ", snap,subhalo)
        if cond:
            merge = True
            break
        if verbose > 1:
            print("no merger, snap is now",snap)
    if merge:
        return snap_old,subhalo_old
    else:
        return 100,0

# now need to extract histories from snap_first to last time before subhalo destruction

def SubhaloScaleRadius(subhalo,sim='TNG50-1',snap=99):
    return getsubhalo(subhalo,sim,snap,parameter='vmaxrad')/2.16258

def Density_vir_NFW(x,c):
    """Dimensionless NFW 3D density (for testing) in virial units
    arguments:
        r/r_vir
        c (concentration) = r_vir/r_s
    returns: 
        rho(r) / (M_v/r_v^3)
    """
    
    # author: Gary Mamon
    
    denom = 4* np.pi * (np.log(1+c) - c/(1+c)) * x*(x+1/c)**2
    return 1/denom

def Boundaries(pos,halfWidth,sim="TNG50-1"):
    posmin = FixPeriodic(pos-halfWidth)
    posmax = FixPeriodic(pos+halfWidth)
    return posmin, posmax

def GasMap(sub_sat,snap,sim='TNG100-1',subpastsatmax=10,halfwidthoverrhalf=5,
           nxbins=40,Nmaxvfield=500,h=0.6774,extract=True,plotprefix=None,
           satOnly=True,vfcolor='cyan',alpha=0.4,verbose=0):
    """
    Prepare gas map with velocity field, without plotting
    
    Arguments:
     sub_sat: subhaloID of test satellite
     snap: snapshot number
     sim: TNG simulation (default: 'TNG100-1')
     subpastsatmax: max(subhaloID)-sub_sat (default: 10)
     halfwidthoverrhalf: box half-width in units of half-mass radius of all particles
                          (default: 5)
     nxbins: number of bins in one dimension in map (default: 40)
     Nmaxvfield: max number of velocity field arrows in plot (default: 500)
     h: dimensionless Hubble constant at z=0 (default: 0.6774)
     extract: [boolean] (default: True) 
         True: force extraction of particles
         False: read particle data from disk
     plotprefix: prefix of file (default None)
     verbose: verbosity (default: 0)

    Returns:
        subs (subhalos considered) [N]
        pos_rel (positions of gas particles relative to subhalo) [N,3]
        vel_rel (velocities of gas particles relative to subhalo) [N,3]
        mass (masses of gas particles) [N]
        pos_group_rel (position of group relative to subhalo) [3]
        vel_group_rel (velocity of group relative to subhalo) [3]
        vel_gas_bulk (bulk velocity of all gas particles relative to subhalo) [3]
    """
    # redshift parameters for velocity conversions
    z = zofsnap(snap,sim)
    a = 1/(1+z)
    asqrt = np.sqrt(a)
    
    # satellite
    params_sat = getsubhalo(sub_sat,simulation=sim,snapnum=snap)
    pos_sat = np.array([params_sat['pos_x'],params_sat['pos_y'],params_sat['pos_z']])
    vel_sat = np.array([params_sat['vel_x'],params_sat['vel_y'],params_sat['vel_z']])
    
    # half-width
    rhalf = params_sat['halfmassrad'] / h
    halfwidth = halfwidthoverrhalf * rhalf
    
    # velocity dispersion
    sigmav = params_sat['veldisp']
    
    # group
    groupID = params_sat['grnr']
    params_group = gethalo(groupID,simulation=sim,snapnum=snap)
    pos_group = params_group['GroupPos']
    vel_group = [v/a for v in params_group['GroupVel']]      # km/s
    # M200c = 1e10/h*params_group['Group_M_Crit200'] # M_sun
    Nsubs = params_group['GroupNsubs']
    print("Nsubs = ", Nsubs)
    
    # central
    sub_cen = params_group['GroupFirstSub']
    params_cen = getsubhalo(sub_cen,simulation=sim,snapnum=snap)
    pos_cen = np.array([params_cen['pos_x'],params_cen['pos_y'],params_cen['pos_z']])
    vel_cen = np.array([params_cen['vel_x'],params_cen['vel_y'],params_cen['vel_z']])
    
    # gas particles of all subhalos within sub_sat + subpastsatmax
    # . initialize
    pos_rel_all = np.empty(shape=(0,3))
    vel_rel_all = np.empty(shape=(0,3))
    mass_all = np.empty(shape=0)
    subs_all = np.empty(shape=0)
    vel_gas_bulk = np.zeros(3)
    # loop over subhalos from central to final
    for sub in range(sub_cen,sub_cen+Nsubs):
        if sub == sub_cen:
            print("\ncentral")
        elif sub == sub_sat:
            print("\ntest satellite")
        print("sub", sub,"...")
        
        # stop after final desired subhalo
        if sub > sub_sat + subpastsatmax:
            break
        params_sub =  getsubhalo(sub_sat,simulation=sim,snapnum=snap)
        pos_sub = np.array([params_sub['pos_x'],params_sub['pos_y'],params_sub['pos_z']])
        # vel_sub = np.array([params_sub['vel_x'],params_sub['vel_y'],params_sub['vel_z']])
    
        # filter subhalos to be in cube
        pos_rel_sub = FixPeriodic(pos_sub-pos_sat,sim) / h # comoving kpc
        if np.any(np.abs(pos_rel_sub) > halfwidth):
            continue
        
        # extract particles
        if verbose > 0:
            print("particles ...")
        parts = particles(sub,sim=sim,snapnum=snap,PartType=0,extract=extract)
        if len(parts.keys()) == 0:
            print("empty key, moving to next sub...")
            continue
        elif verbose > 0:
            print("found", len(parts['Masses']),"gas particles")
        pos_parts = parts['Coordinates']
        vel_parts = [asqrt * v for v in parts['Velocities']] # comoving km/s
        mass_parts = parts['Masses']
        
        # save subhalos of particles
        sub_parts = 0*mass_parts + sub
        subs_all = np.append(subs_all,sub_parts)
        if verbose > 0:
            print("past pos vel mass...")
        
        # bulk velocity of test satellite gas particles
        if sub == sub_sat:
            # vel_gas_bulk = np.sum(mass_parts[:,None]*vel_parts,axis=0) \
            #     / np.sum(mass_parts)
            # print("vel_bulk_sat-gas-1=",vel_gas_bulk)
            vel_gas_bulk = np.average(vel_parts,axis=0,weights=mass_parts) \
                - vel_sat
            print("vel_bulk_sat-gas-2=",vel_gas_bulk)

        # relative positions and velocities in subhalo frame
        pos_rel = FixPeriodic(pos_parts-pos_sat,sim) / h # comoving kpc
        pos_rel_all = np.append(pos_rel_all,pos_rel)
        pos_rel_all.reshape((-1,3))
        vel_rel = vel_parts - vel_sat
        vel_rel_all = np.append(vel_rel_all,vel_rel)
        vel_rel_all.reshape((-1,3))
        mass_all = np.append(mass_all,mass_parts)
        if verbose > 1:
            print("shapes pos vel mass = ", 
                  pos_rel_all.shape,vel_rel_all.shape,mass_all.shape)
    
    if verbose > 0:
        print("out of subhalo loop")
    pos_rel_all2 = pos_rel_all.reshape(-1,3)
    vel_rel_all2 = vel_rel_all.reshape(-1,3)
    # bulk gas velocity of full cube
    # vel_gas_bulk = np.sum(mass_all[:,None]*vel_rel_all2,axis=0) / np.sum(mass_all)
    
    # 	. one for opposite of direction to central (from FixPeriodic(pos_cen–pos_sat,sim))
    pos_cen_rel = FixPeriodic(pos_cen-pos_sat) / h # comoving kpc
    
    # 	. one for opposite to direction of group motion in test subhalo frame (from vel_cen–vel_sat), with length in same units as those of particle velocity field
    vel_group_rel = vel_group - vel_sat
    
    # filter to be in box width
    cond = (np.abs(pos_rel_all2[:,0])<halfwidth) \
            & (np.abs(pos_rel_all2[:,1])<halfwidth) \
            & (np.abs(pos_rel_all2[:,2])<halfwidth)
    posf = pos_rel_all2[cond]
    velf = vel_rel_all2[cond]
    massf = mass_all[cond]
    subsf = subs_all[cond]
        
    if plotprefix is not None:
        for axes in [[0,1],[0,2],[1,2]]:
            print("\nplotting axes",axes)
            # np.random.seed(123)
            plotGasMap(subsf,posf,velf,massf,pos_cen_rel,vel_group_rel,vel_gas_bulk,
                   sigmav,
                   rhalf,halfwidth,nxbins=nxbins,Nmaxvfield=Nmaxvfield,
                   satOnly=satOnly,vfcolor=vfcolor,
                   alpha=alpha,axes=axes,
                   verbose=verbose,prefix=plotprefix)
    return [subsf, posf, velf, massf, pos_cen_rel, vel_group_rel, vel_gas_bulk, 
            sigmav, rhalf, halfwidth]

def plotGasMap(subsf,posf,velf,massf,pos_cen_rel,vel_group_rel,vel_gas_bulk,
               sigmav,rhalf,halfwidth,nxbins=40,Nmaxvfield=500,
               satOnly=True,vfcolor='cyan',alpha=0.4,
               axes=[0,1], verbose=0,
               prefix=None):    

    # filter to limit number of vectors
    np.random.seed(123)
    if len(posf) > Nmaxvfield:
        print("len pos (filtered) = ", len(posf))
        i = range(len(posf))
        iGood = np.random.choice(i,size=Nmaxvfield,replace=False)
        posf2 = posf[iGood]
        velf2 = velf[iGood]
        massf2 = massf[iGood]
        subsf2 = subsf[iGood]
    else:
        posf2 = posf
        velf2 = velf
        massf2 = massf
        subsf2 = subsf
    
    # figure
    plt.figure(figsize=[6,6])
    ax = plt.gca()
    
    # * Build surface densities of gas in grid inside cube viewed along viewing axis
    # * Gas map
    ax0 = axes[0]
    ax1 = axes[1]
    print("ax0 =",ax0,"ax1 =",ax1)
    plt.hist2d(posf[:,ax0],posf[:,ax1],weights=np.log10(massf),bins=[nxbins,nxbins],
               cmap='pink')
    print("past hist2d")
    
    # * Velocity field
    print("velocity field for",len(posf2),"points")
    
    if not satOnly:
        ## not in sat or central
        print("v-field other...")
        i = np.arange(len(posf2)).astype(int)
        iOther= np.argwhere((subsf2 != sub_sat) & (subsf2 > subsf[0]))
        print("len(iOther)=",len(iOther))
        if len(iOther) > 0:
            if alpha == 0:
                alphaOther = alphaGasMap(len(iOther))
            else:
                alphaOther = alpha    
            plt.quiver(posf2[iOther,ax0],posf2[iOther,ax1],
                       velf2[iOther,ax0],velf2[iOther,ax1],
                       scale=50,scale_units='xy',
                       color='purple',alpha=alphaOther,label='other particles')
        
        # > central
        print("v-field center...")
        iCen = np.argwhere(subsf2 == subsf[0])
        print("len(iCen)=",len(iCen))
        if len(iCen) > 0:
            if alpha == 0:
                alphaCen = alphaGasMap(len(iCen))
            else:
                alphaCen = alpha
            print("max v0 v1 = ",velf2[iCen,ax0].max(),velf2[iCen,ax1].max())
            print("median v0 v1 = ",np.median(velf2[iCen,ax0]),np.median(velf[iCen,ax1]))
            print("median v = ",np.median(np.sqrt(velf2[iCen,ax0]**2+velf[iCen,ax1]**2)))
            plt.quiver(posf2[iCen,ax0],posf2[iCen,ax1],
                       velf2[iCen,ax0],velf2[iCen,ax1],
                       scale=50,scale_units='xy',
                       color='gray',alpha=alphaCen,label='central particles')
    
    # > test satellite
    print("v-field test satellite...")
    iSat= np.argwhere(subsf2 == sub_sat)
    print("len(iSat)=",len(iSat))
    if len(iSat) > 0:
        if alpha == 0:
            alphaSat = alphaGasMap(len(iSat))
        else:
            alphaSat = alpha
        print("max v0 v1 = ",velf2[iSat,ax0].max(),velf2[iSat,ax1].max())
        print("median v0 v1 = ",np.median(velf2[iSat,ax0]),np.median(velf[iSat,ax1]))
        print("median v = ",np.median(np.sqrt(velf2[iSat,ax0]**2+velf[iSat,ax1]**2)))
        if satOnly:
            plt.quiver(posf2[iSat,ax0],posf2[iSat,ax1],
                       velf2[iSat,ax0],velf2[iSat,ax1],
                       color=vfcolor,alpha=alphaSat,label='satellite gas velocities')
        else:
            plt.quiver(posf2[iSat,ax0],posf2[iSat,ax1],
                   velf2[iSat,ax0],velf2[iSat,ax1],
                   scale=50,scale_units='xy',
                   color='royalblue',alpha=alphaSat,label='satellite gas velocities')
        v = np.transpose([velf2[iSat,ax0].flatten(),velf2[iSat,ax1].flatten()])
        m = massf2[iSat].flatten()
        print("new bulk sat v_proj gas of selected = ",np.average(v,axis=0,weights=m))
    print("past particle v-fields")
    
    # superpose 2 vectors
    pos1 = np.array([0,0,0])
    pos3 = np.array([pos1,pos1])
    print("pos3=",pos3)
    # velocity in units of sigma_v/5
    vel3 = 25/sigmav * \
        np.array([vel_group_rel[0:3],vel_gas_bulk[0:3]])
    print("vel3=",vel3)
    colors = ['g','b']
    labels = ['group velocity (ram pressure)',
              'satellite gas bulk velocity']
    print("2nd quiver")
    for i in range(2):
        print("i ax0 ax1=",i,ax0,ax1)
        plt.quiver(pos3[i,ax0],pos3[i,ax1],
                   vel3[i,ax0],vel3[i,ax1],
                   color=colors[i],
                   label=labels[i])
        
    # radial arrow
    plt.quiver(pos1[ax0],pos1[ax1],-pos_cen_rel[ax0],-pos_cen_rel[ax1],
               color='r',label='direction away from central (AGN-cen)')
    print("done")
    
    # add circles
    print("circles...")
    for i, n in enumerate([1,2,halfwidthoverrhalf]):
        rad = n*rhalf
        print("n rad = ",n,rad)
        circle = plt.Circle((0, 0), radius=rad, color='k', fill=False, ls='-')
        ax.add_artist(circle)
        
    # * plot limits –5 r_1/2, 5_r_1/2, so that  at center of test subhalo
    plt.xlim([-halfwidth,halfwidth])
    plt.ylim([-halfwidth,halfwidth])
    axlabels = ['x','y','z']
    plt.xlabel('$' + axlabels[ax0] + '\ \mathrm{(kpc)}$')
    plt.ylabel('$' + axlabels[ax1] + '\ \mathrm{(kpc)}$')
    plt.grid()
    plt.legend(loc='upper left',fontsize=12,title=r'{\bf velocities in satellite frame}',
               title_fontsize=12)
    plt.title(sim + '$\ \ \ \mathrm{snapnum}\ $' + str(snap) + '$\ \ \ \mathrm{subhalo}\ $' + str(sub_sat), fontsize=14)
    if prefix is None:
        return
    if prefix == 'auto':
        file = "GasMap_" + str(sub_sat) + '_' + axlabels[ax0] + axlabels[ax1] + ".pdf"
    else:
        file = prefix + '_' + axlabels[ax0] + axlabels[ax1] + ".pdf"
    print("printing to",file)
    plt.savefig(file)
    
def alphaGasMap(N):
    return min(1,np.sqrt(400/N))

# sys.exit(0)

# # * For test subhalo:

# sim = 'TNG100-1'
# # sub_sat = 48524
# # snap = 58
# sub_sat = 49939
# snap = 59
# # sub_sat= 50252
# # snap = 60
# # sub_sat = 47361
# # snap = 57
# # sub_sat = 364213
# # snap = 84
# # h = 0.6774
# halfwidthoverrhalf = 3
# nxbins = 40
# alpha = 0.4
# # nybins = 200
# Nmaxvfield = 2000
# extract = False
# verbose = 1

# # extract particles
# [subsf, posf, velf, massf, pos_cen_rel, vel_group_rel, vel_gas_bulk, sigmav, 
#     rhalf, halfwidth] = \
#     GasMap(sub_sat,snap,sim=sim,halfwidthoverrhalf=halfwidthoverrhalf,
#            nxbins=nxbins,alpha=0,verbose=verbose,extract=extract,plotprefix='auto')

# print("past GasMap")

# # stop here
# sys.exit(0)

# ### TO DO: OPTION TO ONLY SHOW SAT GAS VELOCITY FIELD
# ### OR SMALLER FRACTION OF CENTRAL VELS

# # other plots

# # replot (after modif to PlotGasMap)

# for axes in [[0,1],[0,2],[1,2]]:
#     print("\nplotting axes",axes)
#     plotGasMap(subsf,posf,velf,massf,pos_cen_rel,vel_group_rel,vel_gas_bulk,
#            sigmav,rhalf,halfwidth,nxbins=nxbins,Nmaxvfield=Nmaxvfield,
#            alpha=0,axes=axes,
#            verbose=verbose,prefix='test')
    
# # different alpha
# for axes in [[0,1],[0,2],[1,2]]:
#     print("\nplotting axes",axes)
#     plotGasMap(subsf,posf,velf,massf,pos_cen_rel,vel_group_rel,vel_gas_bulk,
#            sigmav,rhalf,halfwidth,nxbins=nxbins,Nmaxvfield=Nmaxvfield,
#            alpha=0.4,axes=axes,
#            verbose=verbose,prefix='test')
# # zoom in
# for axes in [[0,1],[0,2],[1,2]]:
#     print("\nplotting axes",axes)
#     plotGasMap(subsf,posf,velf,massf,pos_cen_rel,vel_group_rel,vel_gas_bulk,
#            sigmav,rhalf,0.2*halfwidth,nxbins=nxbins,Nmaxvfield=Nmaxvfield,
#            alpha=0,axes=axes,
#            verbose=verbose,prefix='zoomin')

# # lower resolution map (and alpha=0.4)
# for axes in [[0,1],[0,2],[1,2]]:
#     print("\nplotting axes",axes)
#     plotGasMap(subsf,posf,velf,massf,pos_cen_rel,vel_group_rel,vel_gas_bulk,
#            sigmav,rhalf,halfwidth,nxbins=int(nxbins/4),Nmaxvfield=Nmaxvfield,
#            alpha=0.5,axes=axes,
#            verbose=verbose,prefix='lowres')
