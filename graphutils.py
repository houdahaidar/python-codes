import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.ticker import MaxNLocator
import numpy as np
import langutils as lu

plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Computer Modern Roman'] \
    + plt.rcParams['font.serif']
    
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

#-------------------------------------------------------------

def plot2(xvec,yvec,xlog=0,ylog=0,xerrvec=None,yerrvec=None,xlims=None,ylims=None,
          size=None,grid=1,xlab=None,ylab=None,labsize=20,ticklabsize=16,maxmajticks=6,leg=None,tit=None,dev=None):
    """scatter plot with automatic limits and axis labels
    authors: Eliott and Gary Mamon
    arguments:
        xvec:        np.array of x-values or np.array of np.arrays of x-values
        yvec:        np.array of y-values or np.array of np.arrays of y-values
        xerrvec:     np.array of x-value errors or np.array of np.arrays of x-value errors
        yerrvec:     np.array of y-value errors or np.array of np.arrays of y-value errors
        xlims:       list of min and max x for plot limits
        ylims:       list of min and max y for plot limits
        size:        sizes of plot markers (default: 2000/len(xvec) in range 0,360)
        grid:        0 --> no grid, 1: major grid, 2: major and minor grids
        xlab:        xlabel (e.g. '$x_2$' for x subscript 2 in LaTeX mode)
        ylab:        ylabel (same syntax)
        labsize:     label size (default 20)
        ticklabsize: tick number label size (default 16)
        maxmajticks: maximum number of major ticks per axis (default 6)
        leg:         plot legend
        tit:         plot title
        dev:         device (None for screen, 'test' for 'test.pdf', else string)
"""
    plt.rc('text', usetex=True)
    fig,ax = plt.subplots(figsize=(6,6))  # inner ticks on all 4 sides  

    # log axes if necessary
    if xlog:
        plt.xscale('log')
    if ylog:
        plt.yscale('log')

    # automatic point size according to length of array
    if size is None:
        size = 2000/len(xvec)
    smin=0
    smax=360
    if size > smax:
        size=smax
    if size < smin:
        size=smin
    # add minor ticks
    plt.minorticks_on()

    ## automatic plot limits (going slightly beyond min-max of data)
    if xlims is not None:
        plt.xlim(xlims)
    if ylims is not None:
        plt.ylim(ylims)
        
    # add grid if desired
    if grid >= 1:
        plt.grid(which='major',color='gray',linestyle='--',axis='both')
        if grid >= 2:
            plt.grid(which='minor',color='gray',linestyle=':',axis='both')

    # TeX labels
    Nxvecs = xvec.ndim
    # scatter plot for list of xvec,yvec or for single xvec,yvec
    mycolors = ['k','r','g','b','c','m','orange']
    if Nxvecs > 1:
        xoffset = (1,1)
        for i in range(Nxvecs):
            if len(yvec[i]) > 0:
                plt.scatter(xoffset[i]*xvec[i], yvec[i], s=size, c=mycolors[i], label=leg[i])
                plt.errorbar(xoffset[i]*xvec[i],yvec[i],xerr=xerrvec[i],yerr=yerrvec[i],c=mycolors[i],ls='none',elinewidth=2)
    else:
        plt.scatter(xvec, yvec, s=size, c='r', label=leg)
        plt.errorbar(xvec,yvec,xerr=xerrvec,yerr=yerrvec,c='r',ls='none',elinewidth=2)

    # automatic axis labels
    if xlab is None and Nxvecs==1:
        xlab = lu.getvarname(xvec)
        print("xlab is now", xlab)
    if ylab is None and Nxvecs==1:
        ylab = lu.getvarname(yvec)
    plt.xlabel(xlab,fontsize=labsize)
    plt.ylabel(ylab,fontsize=labsize)

    # tickmarks and number labels
    plt.tick_params(labelsize=ticklabsize)
    ax.xaxis.set_major_locator(MaxNLocator(maxmajticks, prune="lower"))
    ax.yaxis.set_major_locator(MaxNLocator(maxmajticks, prune="lower"))

    if leg is not None:
        plt.legend(loc='best')
    if tit is not None:
        plt.title(tit)

    # choose plot style from argument
    # plt.style.use(style)

    # show full plot or save to device
    if dev is None:
        plt.show()
    else:
        if dev.find('.pdf')==len(dev)-4:
            dev2 = dev[0:len(dev)-4]
        else:
            dev2 = dev
        fig.savefig("%s.pdf" % dev2, bbox_inches='tight')        
